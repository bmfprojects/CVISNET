'use strict';

var app = angular.module('brgyapp');

app.controller('taxCtrl', function($scope, DbCollection, Restangular, $resource, $http, $q, $filter, ngTableParams, Tax, $location){

$scope.taxcollectionhead = [
 {list : 'Barangay Tax List'},
 {form : 'Barangay Tax Form'},
];

$scope.brgytax = Restangular.all("taxcollection").getList().$object;


 $http.get(DbCollection + 'taxcollection')

     .then(function(result){

       $scope.tax = result.data


       return $scope.tableParams = new ngTableParams({

          page: 1,            // show first page
          count: 5           // count per page
      }, {
          total: $scope.tax.length, // length of data
          getData: function($defer, params) {
              // use build-in angular filter
              var orderedData = params.sorting() ?
                      $filter('orderBy')($scope.tax, params.orderBy()) :
                      $scope.tax;

                 $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }

      });
    });


  $scope.show = function(id){
    $location.url('/edittax/' + id);
    console.log(id);
  };
  
});
app.controller('addCtrl', function($scope, Restangular, $routeParams, Tax, $location){

$scope.taxcollectionhead = [
 {list : 'Barangay Tax List'},
 {form : 'Barangay Tax Form'},
];


$scope.save = function() {
      Restangular.all('taxcollection').post($scope.tax).then(function(tax) {
      $location.path('/taxlist');
    });
  }

});

app.controller('editCtrl',function($scope, Restangular, $http, $routeParams, Taxis, Tax, $location){


$scope.taxcollectionhead = [
 {list : 'Barangay Tax List'},
 {form : 'Barangay Tax Form'},
];

 $scope.taxis = Taxis;

 $scope.tax = Restangular.copy($scope.taxis);

  $scope.save = function() {
    $scope.tax.put().then(function() {
      $location.path('/taxlist');
    });
  };

  $scope.delete = function() {
      $scope.tax.remove().then(function() {
      $location.path('/taxlist');
    });
  };

});

