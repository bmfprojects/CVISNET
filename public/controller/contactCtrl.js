'use strict';

var app = angular.module('brgyapp');

app.controller('contactCtrl', function($scope, Restangular, $resource, $location, Contact, $http, $q, $routeParams){

$scope.contacts = Restangular.all("taxcollection").getList().$object;

	$scope.fields = ['firstName', 'lastName'];

	$scope.sort = function (field){
		$scope.sort.field = field;
		$scope.sort.order = !$scope.sort.order;
	};

	$scope.sort.field = 'firstName';
	$scope.sort.order = false;

	$scope.show = function(id){
		$location.url('/contact/' + id);
		console.log(id);
	};
});

app.controller('NewContactCtrl', function($scope, Contact, $location){

	$scope.contact = new Contact({
		firstName : ['', 'text'],
		lastName : ['', 'text'],
		email : ['', 'email'],
		homePhone : ['', 'tel'],
		cellPhone : ['', 'tel'],
		date : ['', 'date'],
		address : ['', 'text'],
		website: ['', 'url']
})

$scope.save = function(){

	if ($scope.newContact.$invalid){
		$scope.$broadcast('record:invalid');
	}else{

		$scope.contact.$save();
		$location.url('/contacts');

	}
}

});
app.controller('editContactCtrl', function($scope, $resource, Restangular, Contact, $location, $routeParams){


  $scope.getcontact = Contact;

  $scope.contact = Restangular.copy($scope.getcontact);

  $scope.save = function() {
    $scope.contact.put().then(function() {
      $location.path('/contacts');
    });
  };

  $scope.delete = function() {
      $scope.contact.remove().then(function() {
      $location.path('/contacts');
    });
  };

});