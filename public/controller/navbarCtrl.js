'use strict';

var app = angular.module('brgyapp');

app.controller('navbarCtrl', function($rootScope, $modal, DbCollection, $http, $scope, Restangular){

	  $http.get(DbCollection + 'account/')
      .then(function(result){
      $rootScope.UserAcount = result.data;
        if(result.data){
            if($rootScope.UserAcount.accesscontrol === 'municipalitycontrol'){

              var id = $rootScope.UserAcount.municipality._id;

              $http.get(DbCollection + 'brgycol/'+ id)
                .then(function(result){
                  $rootScope.count_brgy = result.data.length;
                });
          }
        }else{
         // nothing..
        }



      });


$scope.$watch('qrvalue', function(value){


if(value){
var l = value.length;
var string_l = l.toString();

if(string_l === '24'){

    $http.get(DbCollection + '/get_qr_resident/'+ value)
    .then(function (result) {
           $scope.viewqr('lg', value);
    }, function (error) {
          $scope.error = error.data.message;
    });

}else if( l < 24){

  $scope.error = 'Please enter the 24 character!';

}else if(l > 24){

  $scope.error = 'The data length is exceed!'
  
}


}
       $scope.viewqr = function (size, value) {
         
            var modalInstance = $modal.open({
              templateUrl: '../views/viewresident.html',
              controller: $scope.viewresident,
              size: size,
               resolve: {
                      getvalue: function($http){
                          if(value){
                            return $http.get(DbCollection + '/get_qr_resident/'+ value);
                          }else{
                            return null;
                            
                          }
                        }
                      }
            });


            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
                // $log.info('Modal dismissed at: ' + new Date());
                 window.location.reload();
              });

      };

    $scope.viewresident = function($scope, $modalInstance, getvalue, $modal, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {

     $scope.brgyresident = getvalue.data;

          $scope.ok = function () {
            $modalInstance.dismiss('cancel');
            $scope.qrvalue = '';
            window.location.reload();
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
            $scope.qrvalue = '';
            window.location.reload();
          };

    }

})

     $scope.account_modal = function (size, id) {
       
          var modalInstance = $modal.open({
            templateUrl: '../views/my_account.html',
            controller: $scope.editmodal,
            size: size,
             resolve: {
                    getaccount: function($http){
                        if(id){
                          return $http.get(DbCollection + '/registered/'+ id);
                        }else{
                          return null;
                          
                        }
                      },
                      auth: function ($q, authenticationSvc) {
                          var userInfo = authenticationSvc.getUserInfo();
                          if (userInfo) {
                              return $q.when(userInfo);
                          } else {
                              return $q.reject({ authenticated: false });
                          }
                      }
                    }
          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
              window.location.reload();
            });

    };

     $scope.editmodal = function($scope, $modalInstance, getaccount, $modal, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {
     
     	$scope.account = getaccount.data;

          $scope.ok = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

     	   $scope.caption_pass = 'Change Password';

          $scope.enable_new = function (){
            $scope.newpass = 'true';
            $scope.caption_pass = 'Click to hide';
          }
          $scope.enable_hide = function (){
            $scope.newpass = 'false';
            $scope.caption_pass = 'Change Password';
          }

          $scope.update_my_account = function (id, access) {

           if(access === 'user'){

              $http.put(DbCollection + '/edit_own_account/'+id, $scope.account)
                .then(function (result) {
                      $scope.account = result.data;
                      $modalInstance.dismiss('cancel');
                }, function (error) {
                        $scope.error = error.data.message;
                });

           }else if(access === 'municipalitycontrol'){

                $http.put(DbCollection + '/municipalityaccount/'+id, $scope.account)
                .then(function (result) {
                      $scope.account = result.data;
                      $modalInstance.dismiss('cancel');
                }, function (error) {
                        $scope.error = error.data.message;
                });
           }else{

              $http.put(DbCollection + '/registered/'+id, $scope.account)
                .then(function (result) {
                      $scope.account = result.data;
                      $modalInstance.dismiss('cancel');
                }, function (error) {
                        $scope.error = error.data.message;
                });
           }
	   
	        };
     };


});