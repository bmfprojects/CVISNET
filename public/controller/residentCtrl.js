'use strict';

var app = angular.module('brgyapp');

app.controller('residentCtrl', function($scope, $window, $rootScope, $modal, myAccount, $http, Restangular, ngTableParams, $q, $filter, DbCollection, $location){
   
// check if the UserAcount is exist, if not clear the sessionStorage
   $http.get(DbCollection + 'account/')
    .then(function(result){
      $rootScope.UserAcount = result.data;
      if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
        sessionStorage.clear();
        $location.path('/login');
        window.location.reload();
      }
    });
 $http.get(DbCollection + 'registered/')
     .then(function(result){
       $scope.useraccount = result.data
      $rootScope.account_count = $scope.useraccount.length
  })

    $http.get(DbCollection + 'resident/')
     .then(function(result){
       $scope.resident = result.data
       var people = $scope.resident.length
       $rootScope.people = people;

       return $scope.tableParams = new ngTableParams({

          page: 1,            // show first page
          count: 5           // count per page
          
      }, {
          total: $scope.resident.length, // length of data
          getData: function($defer, params) {
              // use build-in angular filter
              var orderedData = params.sorting() ?
                      $filter('orderBy')($scope.resident, params.orderBy()) :
                      $scope.resident;

                 $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
      });
    });


    $scope.destroy = function (size, id, f, m, l) {
       
          var modalInstance = $modal.open({
            templateUrl: '../views/deleteresident.html',
            controller: $scope.deleteme,
            size: size,
             resolve: {
                    getdelete: function(){
                           var contentdata = {
                            id : id,
                            f : f,
                            m : m,
                            l : l
                           };
                           return contentdata;
                      }
                    }
          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
               window.location.reload();
            });

    };
 $scope.deleteme = function($scope, $modalInstance, getdelete, $modal, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {
  $scope.getdelete = getdelete;

      $scope.yes = function(id, f, m, l){

                  $http.delete(DbCollection+'/resident/'+ id)
                  window.location.reload();
  
        };

      $scope.ok = function () {
        $modalInstance.dismiss('cancel');
      };

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };
  }

  $scope.show = function(id){
    $location.url('/resident/' + id);
  };

    $scope.residentModel = function (size) {
       
          var modalInstance = $modal.open({
            templateUrl: '../views/resident.html',
            controller: $scope.addmodel,
            size: size
          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
               window.location.reload();
            });

    };


          $scope.resident_edit_Model = function (size, id) {
         
            var modalInstance = $modal.open({
              templateUrl: '../views/resident.html',
              controller: $scope.model,
              size: size,
              resolve: {
                    getresident: function($http){
                        if(id){
                          return $http.get(DbCollection + '/resident/'+ id);
                        }else{
                          return null;
                          
                        }
                      }
                    }
            });


            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
                // $log.info('Modal dismissed at: ' + new Date());
                 window.location.reload();
              });

      };

  $scope.addmodel = function($scope, $modalInstance, $modal, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {
              



  $scope.disabled = undefined;
  $scope.searchEnabled = undefined;

  $scope.enable = function() {
    $scope.disabled = false;
  };

  $scope.disable = function() {
    $scope.disabled = true;
  };

  $scope.enableSearch = function() {
    $scope.searchEnabled = true;
  }

  $scope.disableSearch = function() {
    $scope.searchEnabled = false;
  }

  $scope.clear = function() {
    $scope.person.selected = undefined;
    $scope.address.selected = undefined;
    $scope.country.selected = undefined;
  };

  $scope.someGroupFn = function (item){

    if (item.name[0] >= 'A' && item.name[0] <= 'M')
        return 'From A - M';

    if (item.name[0] >= 'N' && item.name[0] <= 'Z')
        return 'From N - Z';

  };

  $scope.personAsync = {selected : "wladimir@email.com"};
  $scope.peopleAsync = [];

  $timeout(function(){
   $scope.peopleAsync = [
        { name: 'Adam',      email: 'adam@email.com',      age: 12, country: 'United States' },
        { name: 'Amalie',    email: 'amalie@email.com',    age: 12, country: 'Argentina' },
        { name: 'Estefanía', email: 'estefania@email.com', age: 21, country: 'Argentina' },
        { name: 'Adrian',    email: 'adrian@email.com',    age: 21, country: 'Ecuador' },
        { name: 'Wladimir',  email: 'wladimir@email.com',  age: 30, country: 'Ecuador' },
        { name: 'Samantha',  email: 'samantha@email.com',  age: 30, country: 'United States' },
        { name: 'Nicole',    email: 'nicole@email.com',    age: 43, country: 'Colombia' },
        { name: 'Natasha',   email: 'natasha@email.com',   age: 54, country: 'Ecuador' },
        { name: 'Michael',   email: 'michael@email.com',   age: 15, country: 'Colombia' },
        { name: 'Nicolás',   email: 'nicole@email.com',    age: 43, country: 'Colombia' }
      ];
  },3000);
 //   $scope.people = [];
 // $timeout(function(){
 //       $http.get(DbCollection+'/resident/')
 //              .then(function(result){
 //               $scope.people = result.data;
 //              })
 //  },3000);

  $scope.counter = 0;
  $scope.someFunction = function (item, model){
    $scope.counter++;
    $scope.eventResult = {item: item, model: model};
  };

  $scope.removed = function (item, model) {
    $scope.lastRemoved = {
        item: item,
        model: model
    };
  };

  $scope.tagTransform = function (newTag) {
    var item = {
        name: newTag,
        email: newTag.toLowerCase()+'@email.com',
        age: 'unknown',
        country: 'unknown'
    };

    return item;
  };

  $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultme){
        $scope.children_me = resultme.data;
  });
$scope.children_me = {};
    $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultd){
        $scope.children_d = resultd.data;
  });


  $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultm){
        $scope.children_m = resultm.data;
  });


  $scope.person = {};
  $scope.people = [
    { name: 'Adam',      email: 'adam@email.com',      age: 12, country: 'United States' },
    { name: 'Amalie',    email: 'amalie@email.com',    age: 12, country: 'Argentina' },
    { name: 'Estefanía', email: 'estefania@email.com', age: 21, country: 'Argentina' },
    { name: 'Adrian',    email: 'adrian@email.com',    age: 21, country: 'Ecuador' },
    { name: 'Wladimir',  email: 'wladimir@email.com',  age: 30, country: 'Ecuador' },
    { name: 'Samantha',  email: 'samantha@email.com',  age: 30, country: 'United States' },
    { name: 'Nicole',    email: 'nicole@email.com',    age: 43, country: 'Colombia' },
    { name: 'Natasha',   email: 'natasha@email.com',   age: 54, country: 'Ecuador' },
    { name: 'Michael',   email: 'michael@email.com',   age: 15, country: 'Colombia' },
    { name: 'Nicolás',   email: 'nicolas@email.com',    age: 43, country: 'Colombia' }
  ];

    $scope.availableColors = ['Red','Green','Blue','Yellow','Magenta','Maroon','Umbra','Turquoise'];

  $scope.multipleDemo = {};
  $scope.multipleDemo.colors = ['Blue','Red'];
  $scope.multipleDemo.colors2 = ['Blue','Red'];
  $scope.multipleDemo.selectedPeople = [$scope.people[5], $scope.people[4]];
  $scope.multipleDemo.selectedPeople2 = $scope.multipleDemo.selectedPeople;
  $scope.multipleDemo.selectedPeopleWithGroupBy = [$scope.people[8], $scope.people[6]];
  $scope.multipleDemo.selectedPeopleSimple = ['samantha@email.com','wladimir@email.com'];

              //calculate the age base on the birthdate given.

              $scope.$watch('brgyresident.b_date', function(dateString){
                   // var birthday = +new Date(dateString);
                   // $scope.brgyresident.get_age = ((Date.now() - birthday) / (31557600000));
                    var birthday = new Date(dateString);
                    var ageDifMs = Date.now() - birthday.getTime();
                    var ageDate = new Date(ageDifMs); // miliseconds from epoch
                    $scope.get_age = Math.abs(ageDate.getFullYear() - 1970);

              })

              $scope.ok = function () {
                $modalInstance.dismiss('cancel');
              };

              $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
              };

          $http.get(DbCollection+'/region')
          .then(function(result){
            $scope.r = result.data;
          });

          $scope.$watch('brgyresident.region_bplace._id', function(id){
              $http.get(DbCollection+'/province/'+id)
              .then(function(result){
               $scope.p = result.data;
              })
          });

          $scope.$watch('brgyresident.province_bplace._id', function(id){
              $http.get(DbCollection+'/municipality/'+id)
              .then(function(result){
               $scope.m = result.data;
              })
          });

          $scope.$watch('brgyresident.municipality_bplace._id', function(id){
              $http.get(DbCollection+'/brgycol/'+id)
              .then(function(result){
               $scope.b = result.data;
              })
          });

          $scope.showImageCropper = true;

          var currentmunicipal = $scope.UserAcount.municipality;

          $scope.brgyresident = {
                  upload: []
           };


              $scope.brgy_obj = function(){ 

              $http.get(DbCollection + 'resident/')
              .then(function(result){
              $scope.children_constituents = result.data;

              });

              }

              $scope.brgy_obj();

              $scope.clearance = {
                  children_constituents: []
              };


          $scope.uploadFile = function(){
                var file = $scope.avatar;
                var uploadUrl = "resident/images";
                fileUpload.uploadFileToUrl(file, uploadUrl);
            };

          $scope.brgyresident = {
                    avatar: []
           };


        $scope.languagesfamily = [ 
                 {
                  name: 'English',
                  value: "English"
              }, {
                  name: 'Chinese',
                  value: "Chinese"
              }, {
                  name: 'Pilipino',
                  value: "Pilipino"
              }];

        $scope.languagesdialect = [
                   {
                    name: 'Tagalog',
                    value: "Tagalog"
                }, {
                    name: 'Ilocano',
                    value: "Ilocano"
                }, {
                    name: 'Waray',
                    value: "Waray"
                }, {
                    name: 'Ilongo',
                    value: "Ilongo"
                }, {
                    name: 'Cebuano',
                    value: "Cebuano"
                }];

        $scope.immunizations = [
                    'BCG',
                    'Hepatitis',
                    'TT',
                    'Polio'
                ];

                $scope.brgyresident = {
                    immunizations: []
                };

                $scope.nutritions = [
                    'Home Visit',
                    'Bench Conference',
                    'Mother Class',
                    'Cooking, food preparation',
                    'Pabasa',
                    'BNC Meeting',
                    'Ind. Consultation',
                    'Supplemental Feeding'
                ];

                $scope.brgyresident = {
                    nutritions: []
                };

                $scope.healths = [
                    'Barangay Health Center',
                    'Private Doctors Clinic',
                    'Government Hospitals',
                    'Lot Ownership',
                    'Owned',
                    'Rented',
                    'Leased',
                    'Amortizing'
                ];

                $scope.brgyprofile = {
                    healths: []
                };

                $scope.others = [
                    'House Ownership',
                    'Owned',
                    'Rented',
                    'Leased',
                    'Amortizing',
                    'Others'
                ];

                $scope.brgyresident = {
                    others: []
                };

                $scope.waters = [
                    'MCWD',
                    'Pump',
                    'Artesian Well',
                    'Others',
                ];

                $scope.brgyresident = {
                    waters: []
                };

                $scope.supplies = [
                    'Always available',
                    'Sometimes no water',
                    'Most of the time',
                    'no water available'
                ];

                $scope.brgyresident = {
                    supplies: []
                };

                $scope.facilities = [
                    'Always available',
                    'Sometimes no water',
                    'Most of the time',
                    'no water available'
                ];

                $scope.brgyresident = {
                    facilities: []
                };

                $scope.otherones = [
                    'Garbage Disosal',
                    'Always available',
                    'Sometimes no water',
                    'Most of the time',
                    'no water available',
                    'Others'
                ];

                $scope.brgyresident = {
                    otherones: []
                };

                $scope.languages = [
                    'English',
                    'Tagalog',
                    'Bisayas',
                    'Ilonggo',
                    'Waray',
                    'Cebuano'
                ];

                $scope.brgyresident = {
                    languages: []
                };

                $scope.dialects = [
                    'English',
                    'Tagalog',
                    'Bisayas',
                    'Ilonggo',
                    'Waray',
                    'Cebuano'
                ];

                $scope.brgyresident = {
                    dialects: []
                };

        var img;
                 $scope.take_snapshot = function(data_uri) {
                        // take snapshot and get image data
                        Webcam.snap( function(data_uri) {
                            // display results in page

                            $scope.brgyresident.imageuri = data_uri;
               
                            document.getElementById('capture_image').innerHTML = 
                                // '<h2>Here is your image:</h2>' + 
                                '<img id="myImg" src="'+data_uri+'"/>' +
                                '<input type="hidden" id="resulturi" ng-model="brgyresident.imageuri" value="'+ $scope.brgyresident.imageuri +'" >';
                               
                                // '<input type="text" ng-model="brgyresident.baseurlimage" value="'+data_uri+'" />';
                                
                        });

                            var x = document.getElementById("myImg").src;
                            var x_result = x.toString();
               }

                $scope.saveresident = function() {

                Restangular.all('resident').post($scope.brgyresident).then(function(brgyresident) {
                   $modalInstance.dismiss('cancel');
                   window.location.reload();
                }, function (error) {
                        $scope.error = error.data.message;
                    });
              }

      }

      $scope.viewresidentModel = function (size, id) {
       
          var modalInstance = $modal.open({
            templateUrl: '../views/viewresident.html',
            controller: $scope.model,
            size: size,
            resolve: {
                  getresident: function($http){
                      if(id){
                        return $http.get(DbCollection + '/resident/'+ id);
                      }else{
                        return null;
                        
                      }
                    }
                  }
          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
               window.location.reload();
            });

    };

$scope.model = function($scope, $modalInstance, $modal, getresident, $http, $resource, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {

var resident_childs = $resource(DbCollection + '/resident');
$scope.v = 7;
$scope.zv = 50;
$scope.z = 50;

  $scope.disabled = undefined;
  $scope.searchEnabled = undefined;

  $scope.enable = function() {
    $scope.disabled = false;
  };

  $scope.disable = function() {
    $scope.disabled = true;
  };

  $scope.enableSearch = function() {
    $scope.searchEnabled = true;
  }

  $scope.disableSearch = function() {
    $scope.searchEnabled = false;
  }

  $scope.clear = function() {
    $scope.person.selected = undefined;
    $scope.address.selected = undefined;
    $scope.country.selected = undefined;
  };

  $scope.someGroupFn = function (item){

    if (item.name[0] >= 'A' && item.name[0] <= 'M')
        return 'From A - M';

    if (item.name[0] >= 'N' && item.name[0] <= 'Z')
        return 'From N - Z';

  };

  $scope.personAsync = {selected : "wladimir@email.com"};
  $scope.peopleAsync = [];

  $timeout(function(){
   $scope.peopleAsync = [
        { name: 'Adam',      email: 'adam@email.com',      age: 12, country: 'United States' },
        { name: 'Amalie',    email: 'amalie@email.com',    age: 12, country: 'Argentina' },
        { name: 'Estefanía', email: 'estefania@email.com', age: 21, country: 'Argentina' },
        { name: 'Adrian',    email: 'adrian@email.com',    age: 21, country: 'Ecuador' },
        { name: 'Wladimir',  email: 'wladimir@email.com',  age: 30, country: 'Ecuador' },
        { name: 'Samantha',  email: 'samantha@email.com',  age: 30, country: 'United States' },
        { name: 'Nicole',    email: 'nicole@email.com',    age: 43, country: 'Colombia' },
        { name: 'Natasha',   email: 'natasha@email.com',   age: 54, country: 'Ecuador' },
        { name: 'Michael',   email: 'michael@email.com',   age: 15, country: 'Colombia' },
        { name: 'Nicolás',   email: 'nicole@email.com',    age: 43, country: 'Colombia' }
      ];
  },3000);


  $scope.counter = 0;
  $scope.someFunction = function (item, model){
    $scope.counter++;
    $scope.eventResult = {item: item, model: model};
  };

  $scope.removed = function (item, model) {
    $scope.lastRemoved = {
        item: item,
        model: model
    };
  };

  $scope.tagTransform = function (newTag) {
    var item = {
        name: newTag,
        email: newTag.toLowerCase()+'@email.com',
        age: 'unknown',
        country: 'unknown'
    };

    return item;
  };

  $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultme){
        $scope.children_me = resultme.data;
  });
$scope.children_me = {};
    $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultd){
        $scope.children_d = resultd.data;
  });


  $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultm){
        $scope.children_m = resultm.data;
  });


  $scope.person = {};
  $scope.people = [
    { name: 'Adam',      email: 'adam@email.com',      age: 12, country: 'United States' },
    { name: 'Amalie',    email: 'amalie@email.com',    age: 12, country: 'Argentina' },
    { name: 'Estefanía', email: 'estefania@email.com', age: 21, country: 'Argentina' },
    { name: 'Adrian',    email: 'adrian@email.com',    age: 21, country: 'Ecuador' },
    { name: 'Wladimir',  email: 'wladimir@email.com',  age: 30, country: 'Ecuador' },
    { name: 'Samantha',  email: 'samantha@email.com',  age: 30, country: 'United States' },
    { name: 'Nicole',    email: 'nicole@email.com',    age: 43, country: 'Colombia' },
    { name: 'Natasha',   email: 'natasha@email.com',   age: 54, country: 'Ecuador' },
    { name: 'Michael',   email: 'michael@email.com',   age: 15, country: 'Colombia' },
    { name: 'Nicolás',   email: 'nicolas@email.com',    age: 43, country: 'Colombia' }
  ];

    $scope.availableColors = ['Red','Green','Blue','Yellow','Magenta','Maroon','Umbra','Turquoise'];

  $scope.multipleDemo = {};
  $scope.multipleDemo.colors = ['Blue','Red'];
  $scope.multipleDemo.colors2 = ['Blue','Red'];
  $scope.multipleDemo.selectedPeople = [$scope.people[5], $scope.people[4]];
  $scope.multipleDemo.selectedPeople2 = $scope.multipleDemo.selectedPeople;
  $scope.multipleDemo.selectedPeopleWithGroupBy = [$scope.people[8], $scope.people[6]];
  $scope.multipleDemo.selectedPeopleSimple = ['samantha@email.com','wladimir@email.com'];



$scope.children = [];

$scope.brgyresident = {
  children : []
};



var today = Date.now();

        function generatedata(today){

        var days,
            month;

        var d = new Date(today);
        days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        var today = days[d.getUTCDay()];
        var dd = days[d.getUTCDay()];
        var mm = month[d.getMonth()]; //January is 0!
        var yyyy = d.getFullYear();
        var dy = d.getDate();
     
        var todays = mm+' '+dy+', '+ yyyy;
        return todays;

        };

var getdate_now = generatedata(today);

var getperiodof_time = new Date().toString("hh:mm tt");
console.log(getperiodof_time);

var url_value =  getresident.data._id;

//Generate the QRCode for every full name of the resident
    var qrcode = new QRCode("qrcode", {
                     width : 200,
                     height : 200
                   });

    var sss = qrcode.makeCode(url_value);
   

console.log(url_value);
              $scope.take_snapshot = function(data_uri) {
                // take snapshot and get image data
                Webcam.snap( function(data_uri) {
                    // display results in page

                    $scope.brgyresident.imageuri = data_uri;
       
                    document.getElementById('capture_image').innerHTML = 
                        // '<h2>Here is your image:</h2>' + 
                        '<img id="myImg" src="'+data_uri+'"/>' +
                        '<input type="hidden" id="resulturi" ng-model="brgyresident.imageuri" value="'+ $scope.brgyresident.imageuri +'" >';
                       
                        // '<input type="text" ng-model="brgyresident.baseurlimage" value="'+data_uri+'" />';
                        
                });

                    var x = document.getElementById("myImg").src;
                    var x_result = x.toString();
            }


           $scope.$watch('brgyresident.b_date', function(dateString){
                 // var birthday = +new Date(dateString);
                 // $scope.brgyresident.get_age = ((Date.now() - birthday) / (31557600000));
                  var birthday = new Date(dateString);
                  var ageDifMs = Date.now() - birthday.getTime();
                  var ageDate = new Date(ageDifMs); // miliseconds from epoch
                  $scope.get_age = Math.abs(ageDate.getFullYear() - 1970);

            });

           $http.get(DbCollection + '/clearance/'+getresident.data._id)
             .then(function(result){
               $scope.clearance = result.data

               var no_clearance = $scope.clearance.length
               $rootScope.no_clearance = no_clearance;
               
               return $scope.tableParams = new ngTableParams({

                  page: 1,            // show first page
                  count: 5           // count per page
              }, {
                  total: $scope.clearance.length, // length of data
                  getData: function($defer, params) {
                      // use build-in angular filter
                      var orderedData = params.sorting() ?
                              $filter('orderBy')($scope.clearance, params.orderBy()) :
                              $scope.clearance;

                         $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                  }
              });
            });

          $http.get(DbCollection+'/region')
          .then(function(result){
            $scope.r = result.data;
          });

          $scope.$watch('brgyresident.region_bplace._id', function(id){
              $http.get(DbCollection+'/province/'+id)
              .then(function(result){
               $scope.p = result.data;
              })
          });

          $scope.$watch('brgyresident.province_bplace._id', function(id){
              $http.get(DbCollection+'/municipality/'+id)
              .then(function(result){
               $scope.m = result.data;
              })
          });

          $scope.$watch('brgyresident.municipality_bplace._id', function(id){
              $http.get(DbCollection+'/brgycol/'+id)
              .then(function(result){
               $scope.b = result.data;
              })
          });

          $scope.create_clearance = function (size, id) {
       
          var modalInstance = $modal.open({
            templateUrl: '../views/clearance.html',
            controller: $scope.clearanceCtrl,
            size: size,
            resolve: {
              getclearance: function($http){
                  if(id){
                    return $http.get(DbCollection + '/clearance/'+ id);
                  }else{
                    return null;
                    
                  }
                }
              }

          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
            });

        };

        $scope.edit_clearance = function (size, id) {
       
          var modalInstance = $modal.open({
            templateUrl: '../views/clearance.html',
            controller: $scope.editclearanceCtrl,
            size: size,
            resolve: {
              getclearance: function($http){
                  if(id){
                    return $http.get(DbCollection + '/getclearance/'+ id);
                  }else{
                    return null;
                    
                  }
                }
              }

          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
            });

        };

        $scope.show_clearance = function (size, id) {
       
          var modalInstance = $modal.open({
            templateUrl: '../views/viewclearance.html',
            controller: $scope.editclearanceCtrl,
            size: size,
            resolve: {
              getclearance: function($http){
                  if(id){
                    return $http.get(DbCollection + '/getclearance/'+ id);
                  }else{
                    return null;
                    
                  }
                }
              }

          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
               window.location.reload();
            });

        };

          $scope.editclearanceCtrl = function($scope, $modalInstance, getclearance, $modal, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {
             
              $scope.ok = function () {
                $modalInstance.dismiss('cancel');
                 window.location.reload();
              };

              $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
                 window.location.reload();
              };

            $scope.clearance = getclearance.data;



            $scope.update_clearance = function(cid, rid) {

            $http.get(DbCollection + '/resident/'+ rid)
            .then(function(resultr){
              $scope.clearance.resident = resultr.data;

               $http.put(DbCollection+'/clearance/'+cid, $scope.clearance)
               .then(function(result) {
                    $scope.dataget = result.data;
                    $modalInstance.dismiss('cancel');
                    window.location.reload();
                  }, function(error){
                    $scope.error = error.data.message;
                  });

            });


              };

              $scope.purpose = [ 
                  {name: 'Employment', value: 'Employment'},
                  {name: 'Mariage', value: 'Mariage'},
                  {name: 'Travel', value: 'Travel'},
                  {name: 'For Whatever Purpose', value: 'For Whatever Purpose'}
              ];
              $scope.remark = [ 
                  {name: 'No Derogatory Record', value: 'No Derogatory Record'}
              ];

          }

          $scope.clearanceCtrl = function($scope, $modalInstance, $modal, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {
              
               $http.get(DbCollection + 'account/')
                .then(function(result){
                $rootScope.UserAcount = result.data;
                });
             
              $scope.ok = function () {
                $modalInstance.dismiss('cancel');
              };

              $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
              };
              var referrerId = $rootScope.UserAcount.createdbyId;
               

                $scope.brgy_obj = function(){

                  $http.get(DbCollection + 'resident/')
                  .then(function(result){
                  $scope.children_constituents = result.data;
                  return simulateAjax($scope.children_constituents);
                  });

                }

            $scope.brgyclearance = Restangular.all("clearance").getList().$object;


                  var simulateAjax;

                  simulateAjax = function(result) {

                  var deferred, fn;

                  deferred = $q.defer();
                  fn = function() {
                    return deferred.resolve(result);
                  };
                  $timeout(fn, 3000);
                  return deferred.promise;
                };

                $scope.brgy_obj = function(){

                  $http.get(DbCollection + 'resident/')
                  .then(function(result){
                  $scope.children_constituents = result.data;
                  return simulateAjax($scope.children_constituents);

                });

                }

                $scope.brgy_obj();

                $scope.clearance = {
                    children_constituents: []
                };

                
              $http.get(DbCollection + 'resident/')
                  .then(function(result){
                  return  $scope.children_constituents = result.data;
                  });


        $scope.save = function() {

              $scope.clearance.resident = getresident.data;

              $http.post(DbCollection +'/clearance', $scope.clearance)
              .then(function(result){
                $scope.dataget = result.data;
                $modalInstance.dismiss('cancel');
                window.location.reload();
              }, function(error){
                $scope.error = error.data.message;
              });
       }


              $scope.purpose = [ 
                  {name: 'Employment', value: 'Employment'},
                  {name: 'Mariage', value: 'Mariage'},
                  {name: 'Travel', value: 'Travel'},
                  {name: 'For Whatever Purpose', value: 'For Whatever Purpose'}
              ];
              $scope.remark = [ 
                  {name: 'No Derogatory Record', value: 'No Derogatory Record'}
              ];

          }
   




              $scope.brgy_obj = function(){ 

              $http.get(DbCollection + 'resident/')
              .then(function(result){
              $scope.children_constituents = result.data;

              });

              }

              $scope.brgy_obj();

              $scope.clearance = {
                  children_constituents: []
              };


          $scope.uploadFile = function(){
                var file = $scope.avatar;
                var uploadUrl = "resident/images";
                fileUpload.uploadFileToUrl(file, uploadUrl);
            };

          $scope.brgyresident = {
                    avatar: []
           };


        $scope.languagesfamily = [ 
                 {
                  name: 'English',
                  value: "English"
              }, {
                  name: 'Chinese',
                  value: "Chinese"
              }, {
                  name: 'Pilipino',
                  value: "Pilipino"
              }];

        $scope.languagesdialect = [
                   {
                    name: 'Tagalog',
                    value: "Tagalog"
                }, {
                    name: 'Ilocano',
                    value: "Ilocano"
                }, {
                    name: 'Waray',
                    value: "Waray"
                }, {
                    name: 'Ilongo',
                    value: "Ilongo"
                }, {
                    name: 'Cebuano',
                    value: "Cebuano"
                }];

        $scope.immunizations = [
                    'BCG',
                    'Hepatitis',
                    'TT',
                    'Polio'
                ];

                $scope.brgyresident = {
                    immunizations: []
                };

                $scope.nutritions = [
                    'Home Visit',
                    'Bench Conference',
                    'Mother Class',
                    'Cooking, food preparation',
                    'Pabasa',
                    'BNC Meeting',
                    'Ind. Consultation',
                    'Supplemental Feeding'
                ];

                $scope.brgyresident = {
                    nutritions: []
                };

                $scope.healths = [
                    'Barangay Health Center',
                    'Private Doctors Clinic',
                    'Government Hospitals',
                    'Lot Ownership',
                    'Owned',
                    'Rented',
                    'Leased',
                    'Amortizing'
                ];

                $scope.brgyprofile = {
                    healths: []
                };

                $scope.others = [
                    'House Ownership',
                    'Owned',
                    'Rented',
                    'Leased',
                    'Amortizing',
                    'Others'
                ];

                $scope.brgyresident = {
                    others: []
                };

                $scope.waters = [
                    'MCWD',
                    'Pump',
                    'Artesian Well',
                    'Others',
                ];

                $scope.brgyresident = {
                    waters: []
                };

                $scope.supplies = [
                    'Always available',
                    'Sometimes no water',
                    'Most of the time',
                    'no water available'
                ];

                $scope.brgyresident = {
                    supplies: []
                };

                $scope.facilities = [
                    'Always available',
                    'Sometimes no water',
                    'Most of the time',
                    'no water available'
                ];

                $scope.brgyresident = {
                    facilities: []
                };

                $scope.otherones = [
                    'Garbage Disosal',
                    'Always available',
                    'Sometimes no water',
                    'Most of the time',
                    'no water available',
                    'Others'
                ];

                $scope.brgyresident = {
                    otherones: []
                };

                $scope.languages = [
                    'English',
                    'Tagalog',
                    'Bisayas',
                    'Ilonggo',
                    'Waray',
                    'Cebuano'
                ];

                $scope.brgyresident = {
                    languages: []
                };

                $scope.dialects = [
                    'English',
                    'Tagalog',
                    'Bisayas',
                    'Ilonggo',
                    'Waray',
                    'Cebuano'
                ];

                $scope.brgyresident = {
                    dialects: []
                };

          $scope.ok = function () {
            $modalInstance.dismiss('cancel');
            window.location.reload();
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
             window.location.reload();
          };

         
  $scope.download_pdf = function(id){

        var doc = new jsPDF('landscape');
      
        $http.get(DbCollection+'/getclearance/'+id)
        .then(function(result){

        var getdata     = result.data,

          //uncomment below code to sae the exact filename of the pdf file.
          //name        = prompt('Enter the Filename:'),

          prof_lastname         = getdata.resident.prof_lastname,
          prof_firstname        = getdata.resident.prof_firstname,
          prof_middle_name      = getdata.resident.prof_middle_name,
          prof_status           = getdata.resident.prof_status,
          prof_gender           = getdata.resident.prof_gender,
          b_date                = getdata.resident.b_date,
          region                = getdata.resident.region,
          province              = getdata.resident.province,
          captain               = getdata.resident.captain,
          municipality          = getdata.resident.municipality,
          date                  = getdata.date,
          jud_issuedAT          = getdata.jud_issuedAT,
          restCert_num          = getdata.restCert_num,
          ctc_num               = getdata.ctc_num,
          date_of_issue         = getdata.date_of_issue,
          clearance_control_no  = getdata.clearance_control_no,
          clearnce_no           = getdata._id,
          purpose               = getdata.purpose,
          imageuri              = getdata.resident.imageuri,
          remark                = getdata.remark,
          b_date                = getdata.resident.b_date,
          region_bplace         = getdata.resident.region_bplace,
          province_bplace       = getdata.resident.province_bplace,
          municipality_bplace   = getdata.resident.municipality_bplace,
          barangay_bplace       = getdata.resident.barangay_bplace,
          barangay              = getdata.resident.barangay;

        function generateage(dateString){
          var birthday = new Date(dateString),
          ageDifMs = Date.now() - birthday.getTime(),
          ageDate = new Date(ageDifMs), // miliseconds from epoch
          get_age = Math.abs(ageDate.getFullYear() - 1970),
          n = get_age.toString();
          return n;
        }

        function generatedata(dateString){

        var days,
            month;

        var d = new Date(dateString);
        days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        var today = days[d.getUTCDay()];
        var dd = days[d.getUTCDay()];
        var mm = month[d.getMonth()]; //January is 0!
        var yyyy = d.getFullYear();
        var dy = d.getDate();
     
        var todays = mm+' '+dy+', '+ yyyy;
        return todays;

        };

        var currentdate = new Date(); 
        var datetime = ""  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();


        doc.setFontSize(12);
        doc.text(120, 10, "Republic of the Philippines");
        doc.text(120, 15, 'Municipality/City of ' + municipality.municipality_name);
        doc.setFontSize(17);
        doc.setFontType("bold");
        doc.text(90, 25, 'Sangguniang Barangay ng ' + barangay.barangay_name);

        doc.setDrawColor(255,0,0);
        doc.setLineWidth(1.5);
        doc.line(20, 170, 280, 170);

        doc.setDrawColor(0,0,0);
        doc.setLineWidth(.5);
        doc.line(215, 140, 275, 140);


        doc.setDrawColor(0,0,0);
        doc.setFontSize(17);
        doc.setFont("helvetica");

        doc.setFontType("bold");
        doc.text(110, 35, 'BARANGAY CLEARANCE');

        doc.setFontSize(11);
        doc.setFontType("italic");
        doc.text(35, 50, 'TO WHOM IT MAY CONCERN:');

        doc.setFontSize(11);
        doc.setFont("helvetica");
        doc.text(50, 60, 'This is to certify that the person whose name, picture and signature appear hereon');
        doc.text(35, 65, 'has requested a CERTIFICATION from this office and the result/s is/are listed below:');
        doc.setFontSize(11);
        doc.text(35, 75, 'NAME');
        doc.text(35, 80, 'ADDRESS');
        doc.text(35, 85, 'GENDER');
        doc.text(35, 90, 'CIVIL STATUS');
        doc.text(35, 95, 'BIRTH-DATE');
        doc.text(35, 100, 'BIRTH-PLACE');
        doc.text(35, 105, 'AGE');

        doc.text(70, 75, ' : ');
        doc.text(70, 80, ' : ');
        doc.text(70, 85, ' : ');
        doc.text(70, 90, ' : ');
        doc.text(70, 95, ' : ');
        doc.text(70, 100, ' : ');
        doc.text(70, 105, ' : ');


        doc.text(75, 75, prof_firstname + ' ' + prof_middle_name + ' ' + prof_lastname);
        doc.text(75, 80, barangay.barangay_name +', '+ municipality.municipality_name + ', '+ province.province_name);
        doc.text(75, 85, prof_gender);
        doc.text(75, 90, prof_status);
        doc.text(75, 95, generatedata(b_date));
        doc.text(75, 100, barangay_bplace.barangay_name +', '+ municipality_bplace.municipality_name + ', '+ province_bplace.province_name);
        doc.text(75, 105, generateage(b_date));
        
        var todays = new Date();

        doc.setFont("helvetica")
        doc.text(35, 115, 'Given this');
        doc.text(35, 120, 'Remark');
        doc.text(35, 125, 'Purpose');
     

        doc.text(70, 115, ' : ');
        doc.text(70, 120, ' : ');
        doc.text(70, 125, ' : ');
        doc.text(70, 130, ' : ');
        doc.text(70, 135, ' : ');
        doc.text(70, 140, ' : ');
        doc.text(70, 145, ' : ');
        doc.text(70, 145, ' : ');
        
        var getperiodof_time = new Date().toString("hh:mm tt");


        doc.text(75, 115, generatedata(todays));
        doc.text(75, 120, remark);
        doc.text(75, 125, purpose);
        doc.text(75, 130, clearance_control_no);
        doc.text(75, 135, generatedata(date_of_issue));
        doc.text(75, 140, barangay.barangay_name +', '+ municipality.municipality_name + ', '+ province.province_name);
        doc.text(75, 145, getperiodof_time);

        doc.text(220, 145, 'SIGNATURE OF APPLICANT');

        doc.text(190, 50, 'CLEARANCE ID : '+ clearnce_no);
        doc.text(35, 130, 'Tax Cert. Serial');
        doc.text(35, 135, 'Issued on');
        doc.text(35, 140, 'Issued at');
        doc.text(35, 145, 'Time');

        doc.setFontType('bolditalic');
        doc.text(140, 160, captain);
        doc.setFontType('italic');
        doc.text(140, 165, 'PUNONG BARANGAY');
        doc.text(35, 175, 'Note: Not valid without official dry seal. This Barangay Clearance is valid for 1 year from date of issue.');
           

        var idgetbase = document.getElementById('basegetme').src;
        var idgetbaseme = document.getElementById('imageme').src;
        console.log(idgetbase);

        doc.addImage(idgetbaseme, 'JPEG', 220, 55, 50, 50);
        doc.addImage(idgetbase, 'PNG', 239, 172, 20, 20);

        doc.save(prof_firstname +'.pdf');


        });

  }

        $scope.updateresident = function(id) {
          $http.put(DbCollection + '/resident/'+ id, $scope.brgyresident)
          .then(function (result) {
                $scope.brgyresident = result.data;
                $modalInstance.dismiss('cancel');
                window.location.reload();
          }, function (error) {
                $scope.error = error.data.message;
          });

        };

        $scope.destroy = function(id){

            if(confirm('are you sure you want to delete id '+ id +'?') == true){
                $http.delete(DbCollection+'/resident/'+id)
                $modalInstance.dismiss('cancel');
                window.location.reload();
              
            }else{
                $modalInstance.dismiss('cancel');
            };
        };


        if(getresident){
          return $scope.brgyresident = getresident.data;
        }else{
          return null;
        }



    }
  $scope.view_guest = function (size, id) {
       
          var modalInstance = $modal.open({
            templateUrl: '../views/viewresident.html',
            controller: $scope.viewCtrl,
            size: size,
            resolve: {
                  getresident: function($http){
                      if(id){
                        return $http.get(DbCollection + '/resident/'+ id);
                      }else{
                        return null;
                        
                      }
                    }
                  }
          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
            });

    };
    
 $scope.viewCtrl = function($scope, $modalInstance, $modal, getresident, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {
         
$scope.zv = 50;
$scope.v = 7;
$scope.url_value = DbCollection+'viewresident/'+ getresident.data._id;

      $scope.ok = function () {
        $modalInstance.dismiss('cancel');
      };

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };


        if(getresident){
          return $scope.brgyresident = getresident.data;
        }else{
          return null;
        }
 }

});

app.controller('viewresidentCtrl', function($scope, getbrgyresident, $rootScope, $modal, $http, Restangular, ngTableParams, $q, $filter, DbCollection, $location){
 $scope.brgyresident =  getbrgyresident;
 console.log($scope.brgyresident);
});
