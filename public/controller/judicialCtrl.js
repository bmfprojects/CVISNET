'use strict';

var app =angular.module('brgyapp');

app.controller('judicialCtrl', 	function($scope, $resource, $modal, $rootScope, DbCollection, ngTableParams, $http, $timeout, $q, $filter, Restangular, $location){

// check if the UserAcount is exist, if not clear the sessionStorage
   $http.get(DbCollection + 'account/')
    .then(function(result){
      $rootScope.UserAcount = result.data;
      if($rootScope.UserAcount == null){
        sessionStorage.clear();
        $location.path('/login');
        window.location.reload();
      }
    });

     $http.get(DbCollection + '/judicial')
     .then(function(resultjudiciaL){
       $scope.judicial = resultjudiciaL.data;
       var no_judicial = $scope.judicial.length
       $rootScope.no_judicial = no_judicial;

       return $scope.tableParams = new ngTableParams({

          page: 1,            // show first page
          count: 5           // count per page
      }, {
          total: $scope.judicial.length, // length of data
          getData: function($defer, params) {
              // use build-in angular filter
              var orderedData = params.sorting() ?
                      $filter('orderBy')($scope.judicial, params.orderBy()) :
                      $scope.judicial;

                 $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
      });

    });


    $scope.judicialModel = function (size) {
       
          var modalInstance = $modal.open({
            templateUrl: '../views/judicial.html',
            controller: $scope.judicial_addmodel,
            size: size
          });

          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
            });

    };

    $scope.judicial_Edit_Model = function (size, id) {
       console.log(id);
          var modalInstance = $modal.open({
            templateUrl: '../views/judicial.html',
            controller: $scope.judicial_editmodel,
            size: size,
             resolve: {
                    getjudicial: function($http){
                        if(id){
                          return $http.get(DbCollection + '/judicial/'+ id);
                        }else{
                          return null;
                          
                        }
                      }
                    }
          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
            });

    };

    $scope.judicial_view = function (size, id) {
       console.log(id);
          var modalInstance = $modal.open({
            templateUrl: '../views/viewjudicial.html',
            controller: $scope.judicial_editmodel,
            size: size,
             resolve: {
                    getjudicial: function($http){
                        if(id){
                          return $http.get(DbCollection + '/judicial/'+ id);
                        }else{
                          return null;
                          
                        }
                      }
                    }
          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
            });

    };

	$scope.judicial_editmodel = function($scope, getjudicial, $modalInstance, $modal, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {
$scope.disabled = undefined;
  $scope.searchEnabled = undefined;

  $scope.enable = function() {
    $scope.disabled = false;
  };

  $scope.disable = function() {
    $scope.disabled = true;
  };

  $scope.enableSearch = function() {
    $scope.searchEnabled = true;
  }

  $scope.disableSearch = function() {
    $scope.searchEnabled = false;
  }

  $scope.clear = function() {
    $scope.person.selected = undefined;
    $scope.address.selected = undefined;
    $scope.country.selected = undefined;
  };

  $scope.someGroupFn = function (item){

    if (item.name[0] >= 'A' && item.name[0] <= 'M')
        return 'From A - M';

    if (item.name[0] >= 'N' && item.name[0] <= 'Z')
        return 'From N - Z';

  };

  $scope.personAsync = {selected : "wladimir@email.com"};
  $scope.peopleAsync = [];

  $timeout(function(){
   $scope.peopleAsync = [
        { name: 'Adam',      email: 'adam@email.com',      age: 12, country: 'United States' },
        { name: 'Amalie',    email: 'amalie@email.com',    age: 12, country: 'Argentina' },
        { name: 'Estefanía', email: 'estefania@email.com', age: 21, country: 'Argentina' },
        { name: 'Adrian',    email: 'adrian@email.com',    age: 21, country: 'Ecuador' },
        { name: 'Wladimir',  email: 'wladimir@email.com',  age: 30, country: 'Ecuador' },
        { name: 'Samantha',  email: 'samantha@email.com',  age: 30, country: 'United States' },
        { name: 'Nicole',    email: 'nicole@email.com',    age: 43, country: 'Colombia' },
        { name: 'Natasha',   email: 'natasha@email.com',   age: 54, country: 'Ecuador' },
        { name: 'Michael',   email: 'michael@email.com',   age: 15, country: 'Colombia' },
        { name: 'Nicolás',   email: 'nicole@email.com',    age: 43, country: 'Colombia' }
      ];
  },3000);
 //   $scope.people = [];
 // $timeout(function(){
 //       $http.get(DbCollection+'/resident/')
 //              .then(function(result){
 //               $scope.people = result.data;
 //              })
 //  },3000);

  $scope.counter = 0;
  $scope.someFunction = function (item, model){
    $scope.counter++;
    $scope.eventResult = {item: item, model: model};
  };

  $scope.removed = function (item, model) {
    $scope.lastRemoved = {
        item: item,
        model: model
    };
  };

  $scope.tagTransform = function (newTag) {
    var item = {
        name: newTag,
        email: newTag.toLowerCase()+'@email.com',
        age: 'unknown',
        country: 'unknown'
    };

    return item;
  };

  $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultme){
        $scope.children_me = resultme.data;
  });
$scope.children_me = {};
    $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultd){
        $scope.children_d = resultd.data;
  });


  $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultm){
        $scope.children_m = resultm.data;
  });


  $scope.person = {};
  $scope.people = [
    { name: 'Adam',      email: 'adam@email.com',      age: 12, country: 'United States' },
    { name: 'Amalie',    email: 'amalie@email.com',    age: 12, country: 'Argentina' },
    { name: 'Estefanía', email: 'estefania@email.com', age: 21, country: 'Argentina' },
    { name: 'Adrian',    email: 'adrian@email.com',    age: 21, country: 'Ecuador' },
    { name: 'Wladimir',  email: 'wladimir@email.com',  age: 30, country: 'Ecuador' },
    { name: 'Samantha',  email: 'samantha@email.com',  age: 30, country: 'United States' },
    { name: 'Nicole',    email: 'nicole@email.com',    age: 43, country: 'Colombia' },
    { name: 'Natasha',   email: 'natasha@email.com',   age: 54, country: 'Ecuador' },
    { name: 'Michael',   email: 'michael@email.com',   age: 15, country: 'Colombia' },
    { name: 'Nicolás',   email: 'nicolas@email.com',    age: 43, country: 'Colombia' }
  ];

    $scope.availableColors = ['Red','Green','Blue','Yellow','Magenta','Maroon','Umbra','Turquoise'];

  $scope.multipleDemo = {};
  $scope.multipleDemo.colors = ['Blue','Red'];
  $scope.multipleDemo.colors2 = ['Blue','Red'];
  $scope.multipleDemo.selectedPeople = [$scope.people[5], $scope.people[4]];
  $scope.multipleDemo.selectedPeople2 = $scope.multipleDemo.selectedPeople;
  $scope.multipleDemo.selectedPeopleWithGroupBy = [$scope.people[8], $scope.people[6]];
  $scope.multipleDemo.selectedPeopleSimple = ['samantha@email.com','wladimir@email.com'];
         $scope.ok = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.time1 = new Date();
		$scope.time2 = new Date();
		$scope.time2.setHours(7, 30);
		$scope.showMeridian = true;
  
    	$http.get(DbCollection + 'resident/')
	      .then(function(result){
	      return  $scope.children_constituents = result.data;
	      });

		$scope.provinces = 
		  [{
		      name: 'Cebu',
		      value: "Cebu"
		  }, 
		  {
		      name: 'Lanao del Norte',
		      value: "Lanao del Norte"
		  }, 
		  {
		      name: 'Lanao del Sur',
		      value: "Lanao del Sur"
		  }, 
		  {
		      name: 'Zambuanga del Sur',
		      value: "Zambuanga del Sur"
		  }, 
		  {
		      name: 'Cagayan de Oro',
		      value: "Cagayan de Oro"
		  }];

		$scope.languagesfamily = [{
		      name: 'English',
		      value: "English"
		  }, {
		      name: 'Chinese',
		      value: "Chinese"
		  }, {
		      name: 'Pilipino',
		      value: "Pilipino"
		  }];

		$scope.languagesdialect = [{
		      name: 'Tagalog',
		      value: "Tagalog"
		  }, {
		      name: 'Ilocano',
		      value: "Ilocano"
		  }, {
		      name: 'Waray',
		      value: "Waray"
		  }, {
		      name: 'Ilongo',
		      value: "Ilongo"
		  }, {
		      name: 'Cebuano',
		      value: "Cebuano"
		  }];

		$scope.religions = [{
		      name: 'Roman Catholic',
		      value: "Roman Catholic"
		  }, {
		      name: 'Islam',
		      value: "Islam"
		  }, {
		      name: 'Protestants',
		      value: "Protestants"
		  }, {
		      name: 'Evangelical Christian',
		      value: "Evangelical Christian"
		  }, {
		      name: 'Igleisa ni Cristo',
		      value: "Igleisa ni Cristo"
		  }, {
		      name: 'Jehovahs witness',
		      value: "Jehovahs witness"
		  }
		  ];

		$scope.problems = [
		    'Garbage Disposal', 
		    'Drainage', 
		    'Water', 
		    'Health And Sanitation',
		    'Drug Addiction',
		    'Infrastructure',
		    'Political',
		    'Education',
		    'Safety and Security'
		];

		$scope.brgyjudicial = {
		      problems: []
		  };

		$scope.incomes = [
		      'Below P10K',
		      'P10K - P30K',
		      'P30K - P50K',
		      'More than P50K'
		  ];

		$scope.brgyjudicial = {
		      incomes: []
		  };

		$scope.cottages = [
		      'Food Preservation',
		      'Food Processing',
		      'Handicraft',
		      'Others',
		  ];

		$scope.brgyjudicial = {
		      cottages: []
		  };

		$scope.OEAs = [
		      'Sari - sari Store',
		      'Barbecue Stall',
		      'Meat Stall',
		      'Vegetable Stall',
		      'Fishing Vending',
		      'Others'
		  ];

		$scope.brgyjudicial = {
		            OEAs: []
		        };


  		 $scope.brgyjudicial = getjudicial.data;

  		 $scope.minutes = $scope.brgyjudicial.minutes;
  		 $scope.hours = $scope.brgyjudicial.hours;

		  $scope.updatejudcial = function(judicialId, id) {
			$scope.brgyjudicial.minutes =	$scope.minutes;
			$scope.brgyjudicial.hours =	$scope.hours;

		     
		
			$http.put(DbCollection+'/judicial/'+judicialId, $scope.brgyjudicial)
			.then(function(result){
				$scope.result = result.data;
    			$modalInstance.dismiss('cancel');
				window.location.reload();
			},function (error) {
				$scope.error = error.data.message;
            });

		     $scope.brgyjudicial.respondent.case_type = $scope.brgyjudicial.case_type;

			 $http.put(DbCollection + 'residentset/'+ id, $scope.brgyjudicial.respondent)
             .then(function(result){
              $scope.result2 = result.data;

             });

		  }

		  $scope.deletejudicial = function(id){
		  	   if(confirm('are you sure you want to delete id '+ id +'?') == true){
				  	$http.delete(DbCollection+'/judicial/'+id);
				  	window.location.reload();
        
		      }else{
		         $modalInstance.dismiss('cancel');
		      };

		  }

$scope.judicial_pdf = function(id){

        var doc = new jsPDF('landscape');
      
		        $http.get(DbCollection+'/judicial/'+id)
		        .then(function(result){

		        var getdata  = result.data,

		          //uncomment below code to sae the exact filename of the pdf file.
		          //name        = prompt('Enter the Filename:'),

				// complainant info
				
		          prof_lastname         = getdata.complainant.prof_lastname,
		          prof_firstname        = getdata.complainant.prof_firstname,
		          prof_middle_name      = getdata.complainant.prof_middle_name,
		          prof_status           = getdata.complainant.prof_status,
		          prof_gender           = getdata.complainant.prof_gender,
		          b_date                = getdata.complainant.b_date,
		          region                = getdata.complainant.region,
		          province              = getdata.complainant.province,
		          barangay 			 	= getdata.complainant.barangay,
		          captain               = getdata.complainant.captain,
		          municipality          = getdata.complainant.municipality,
		          clearnce_no           = getdata._id,
		          imageuri              = getdata.complainant.imageuri,
		          b_date                = getdata.complainant.b_date,
		          region_bplace         = getdata.complainant.region_bplace,
		          province_bplace       = getdata.complainant.province_bplace,
		          municipality_bplace   = getdata.complainant.municipality_bplace,
		          barangay_bplace       = getdata.complainant.barangay_bplace,

		         // respondent info
		          respondent_prof_lastname         = getdata.respondent.prof_lastname,
		          respondent_prof_firstname        = getdata.respondent.prof_firstname,
		          respondent_prof_middle_name      = getdata.respondent.prof_middle_name,
		          respondent_prof_status           = getdata.respondent.prof_status,
		          respondent_prof_gender           = getdata.respondent.prof_gender,
		          respondent_b_date                = getdata.respondent.b_date,
		          respondent_region                = getdata.respondent.region,
		          respondent_province              = getdata.respondent.province,
		          respondent_barangay			   = getdata.respondent.barangay,
		          respondent_captain               = getdata.respondent.captain,
		          respondent_municipality          = getdata.respondent.municipality,
		          respondent_imageuri              = getdata.respondent.imageuri,
		          respondent_b_date                = getdata.respondent.b_date,
		          respondent_region_bplace         = getdata.respondent.region_bplace,
		          respondent_province_bplace       = getdata.respondent.province_bplace,
		          respondent_municipality_bplace   = getdata.respondent.municipality_bplace,
		          respondent_barangay_bplace       = getdata.respondent.barangay_bplace,
		          case_type       				   = getdata.case_type;

		        function generateage(dateString){
		          var birthday = new Date(dateString),
		          ageDifMs = Date.now() - birthday.getTime(),
		          ageDate = new Date(ageDifMs), // miliseconds from epoch
		          get_age = Math.abs(ageDate.getFullYear() - 1970),
		          n = get_age.toString();
		          return n;
		        }

		        function generatedata(dateString){

		        var days,
		            month;

		        var d = new Date(dateString);
		        days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		        month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

		        var today = days[d.getUTCDay()];
		        var dd = days[d.getUTCDay()];
		        var mm = month[d.getMonth()]; //January is 0!
		        var yyyy = d.getFullYear();
		        var dy = d.getDate();
		     
		        var todays = mm+' '+dy+', '+yyyy;
		        return todays;

		        };

		        var currentdate = new Date(); 
		        var datetime = ""  
		                + currentdate.getHours() + ":"  
		                + currentdate.getMinutes() + ":" 
		                + currentdate.getSeconds();

		        doc.setFontSize(12);

		        // doc.addImage(imageuri, 'JPEG', 220, 55, 50, 50);
		        // doc.addImage(respondent_imageuri, 'JPEG', 100, 55, 50, 50);

		        //Complainant Details

		        doc.setFontSize(13);
		        doc.text(120, 12, "Republic of the Philippines");
		        doc.text(125, 17, "City of" + ' ' + municipality.municipality_name);
		        doc.text(125, 22, "Barangay" + ' ' + barangay.barangay_name);
		        
		        doc.text(105, 27, "OFFICE OF THE LUPONG TAGAPAMAYA");

		        doc.setDrawColor(255,0,0);
		        doc.setLineWidth(1.5);
		        doc.line(20, 35, 280, 35);

		        doc.addImage(imageuri, 'JPEG', 25, 45, 50, 50);
		        doc.setFontSize(13);
		        doc.text(23, 100, "Complainant: ");
		        doc.setDrawColor(0,0,0);
		        doc.setLineWidth(.5);
		        doc.text(23, 108, prof_lastname + ' ' + prof_firstname + ' ' + prof_middle_name);
		        doc.line(23, 109, 70, 109);
		        doc.text(23, 117, barangay.barangay_name + ', ' + municipality.municipality_name +  ', ' + province.province_name);
		        doc.line(23, 118, 70, 118);
		        
		        doc.addImage(respondent_imageuri, 'JPEG', 220, 45, 50, 50);
		        doc.setFontSize(13);
		        doc.text(218, 100, "Respondent/s: ");
		        doc.text(218, 108, respondent_prof_firstname + ' '  + respondent_prof_middle_name + ' '  + respondent_prof_lastname);
		        doc.line(218, 109, 265, 109);
		        doc.text(218, 117, respondent_barangay.barangay_name + ', ' + respondent_municipality.municipality_name + ', ' + respondent_province.province_name);
		        doc.line(218, 118, 265, 118);

		        doc.setFontSize(13);
		        doc.text(130, 50, "-- AGAINST --");

		        doc.text(110, 70, "Barangay Case No.: ");
		        doc.text(114, 79, clearnce_no);
		        doc.line(112, 80, 180, 80);
		        doc.text(112, 90, "For: ");
		        doc.text(114, 99, case_type);
		        doc.line(112, 100, 180, 100);

		        var todays = new Date();

		        var getperiodof_time = new Date().toString("hh:mm tt");

				var month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		        var d = Date.now();

		        var month = month[d.getMonth()];
		        var result_month = month.toString();
		        console.log(month);

		        var day = d.getDate();
		        var result_day = day.toString();
		        console.log(day);

		        doc.setFontSize(11);
		        doc.text(18, 130, "You are hereby summoned to appear before me in person, together with your witnesseses as scheduled below : ");

		        doc.text(50, 140, "Date :");
		        doc.text(66, 139, generatedata(todays));
		        doc.line(61, 140, 100, 140);
		        doc.text(105, 140, "Time :");
		        doc.text(122, 139, getperiodof_time);
		        doc.line(118, 140, 157, 140);

		        doc.text(18, 150, "Then and there to answer to a complaint made before me, copy of which is attached hereto, for mediation or concilation of your dispute with complaint/s.");

		        doc.text(18, 155, "You are hereby warned that if you refuse or willfully fail to appear in obedience to this summons, you may be barred from filling any arising said complaint.");
		     
		        doc.text(18, 165, "FAIL NOT or else face punishment as for contempt of court.");

		        doc.text(22, 175, "Done this");
		        doc.line(41, 175, 60, 175);
				doc.text(47, 174, result_day);
		        doc.text(62, 175, "day of");
		        doc.line(75, 175, 110, 175);
		        doc.text(83, 174, result_month);
		        doc.text(111, 175, "2015.");

		        doc.text(200, 180, captain);
		        doc.text(190, 185, "Punong Barangay/Lupon Chairman");

		        doc.text(22, 190, "Received by:");
		        doc.line(47, 190, 90, 190);
		        doc.text(22, 196, "Date:");
		        doc.line(47, 196, 90, 196);
		        doc.text(52, 195, generatedata(todays));

		        doc.save(prof_firstname + 'VS'+ respondent_prof_firstname +'.pdf');
		        })
  		}


    }

 	$scope.judicial_addmodel = function($scope, $modalInstance, $modal, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {
  
   // $scope.duringtime = new Date('2014-01-01T18:00:00');

  $scope.disabled = undefined;
  $scope.searchEnabled = undefined;

  $scope.enable = function() {
    $scope.disabled = false;
  };

  $scope.disable = function() {
    $scope.disabled = true;
  };

  $scope.enableSearch = function() {
    $scope.searchEnabled = true;
  }

  $scope.disableSearch = function() {
    $scope.searchEnabled = false;
  }

  $scope.clear = function() {
    $scope.person.selected = undefined;
    $scope.address.selected = undefined;
    $scope.country.selected = undefined;
  };

  $scope.someGroupFn = function (item){

    if (item.name[0] >= 'A' && item.name[0] <= 'M')
        return 'From A - M';

    if (item.name[0] >= 'N' && item.name[0] <= 'Z')
        return 'From N - Z';

  };

  $scope.personAsync = {selected : "wladimir@email.com"};
  $scope.peopleAsync = [];

  $timeout(function(){
   $scope.peopleAsync = [
        { name: 'Adam',      email: 'adam@email.com',      age: 12, country: 'United States' },
        { name: 'Amalie',    email: 'amalie@email.com',    age: 12, country: 'Argentina' },
        { name: 'Estefanía', email: 'estefania@email.com', age: 21, country: 'Argentina' },
        { name: 'Adrian',    email: 'adrian@email.com',    age: 21, country: 'Ecuador' },
        { name: 'Wladimir',  email: 'wladimir@email.com',  age: 30, country: 'Ecuador' },
        { name: 'Samantha',  email: 'samantha@email.com',  age: 30, country: 'United States' },
        { name: 'Nicole',    email: 'nicole@email.com',    age: 43, country: 'Colombia' },
        { name: 'Natasha',   email: 'natasha@email.com',   age: 54, country: 'Ecuador' },
        { name: 'Michael',   email: 'michael@email.com',   age: 15, country: 'Colombia' },
        { name: 'Nicolás',   email: 'nicole@email.com',    age: 43, country: 'Colombia' }
      ];
  },3000);
 //   $scope.people = [];
 // $timeout(function(){
 //       $http.get(DbCollection+'/resident/')
 //              .then(function(result){
 //               $scope.people = result.data;
 //              })
 //  },3000);

  $scope.counter = 0;
  $scope.someFunction = function (item, model){
    $scope.counter++;
    $scope.eventResult = {item: item, model: model};
  };

  $scope.removed = function (item, model) {
    $scope.lastRemoved = {
        item: item,
        model: model
    };
  };

  $scope.tagTransform = function (newTag) {
    var item = {
        name: newTag,
        email: newTag.toLowerCase()+'@email.com',
        age: 'unknown',
        country: 'unknown'
    };

    return item;
  };

  $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultme){
        $scope.children_me = resultme.data;
  });
$scope.children_me = {};
    $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultd){
        $scope.children_d = resultd.data;
  });


  $http.get(DbCollection + 'judicial_resident/')
        .then(function(resultm){
        $scope.children_m = resultm.data;
  });


  $scope.person = {};
  $scope.people = [
    { name: 'Adam',      email: 'adam@email.com',      age: 12, country: 'United States' },
    { name: 'Amalie',    email: 'amalie@email.com',    age: 12, country: 'Argentina' },
    { name: 'Estefanía', email: 'estefania@email.com', age: 21, country: 'Argentina' },
    { name: 'Adrian',    email: 'adrian@email.com',    age: 21, country: 'Ecuador' },
    { name: 'Wladimir',  email: 'wladimir@email.com',  age: 30, country: 'Ecuador' },
    { name: 'Samantha',  email: 'samantha@email.com',  age: 30, country: 'United States' },
    { name: 'Nicole',    email: 'nicole@email.com',    age: 43, country: 'Colombia' },
    { name: 'Natasha',   email: 'natasha@email.com',   age: 54, country: 'Ecuador' },
    { name: 'Michael',   email: 'michael@email.com',   age: 15, country: 'Colombia' },
    { name: 'Nicolás',   email: 'nicolas@email.com',    age: 43, country: 'Colombia' }
  ];

    $scope.availableColors = ['Red','Green','Blue','Yellow','Magenta','Maroon','Umbra','Turquoise'];

  $scope.multipleDemo = {};
  $scope.multipleDemo.colors = ['Blue','Red'];
  $scope.multipleDemo.colors2 = ['Blue','Red'];
  $scope.multipleDemo.selectedPeople = [$scope.people[5], $scope.people[4]];
  $scope.multipleDemo.selectedPeople2 = $scope.multipleDemo.selectedPeople;
  $scope.multipleDemo.selectedPeopleWithGroupBy = [$scope.people[8], $scope.people[6]];
  $scope.multipleDemo.selectedPeopleSimple = ['samantha@email.com','wladimir@email.com'];

		var currentdate = new Date(); 
        var datetime = {
        	hours : currentdate.getHours(),
        	minutes : currentdate.getMinutes()
        }

		$scope.minutes = datetime.minutes;
		$scope.hours = datetime.hours;

		$scope.time1 = new Date();
		$scope.time2 = new Date();
		$scope.time2.setHours(7, 30);
		$scope.showMeridian = true;

	      $scope.ok = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
          
    	$http.get(DbCollection + 'judicial_resident/')
	      .then(function(result){
	      return $scope.children_constituents = result.data;
	      });

		$scope.provinces = 
		  [{
		      name: 'Cebu',
		      value: "Cebu"
		  }, 
		  {
		      name: 'Lanao del Norte',
		      value: "Lanao del Norte"
		  }, 
		  {
		      name: 'Lanao del Sur',
		      value: "Lanao del Sur"
		  }, 
		  {
		      name: 'Zambuanga del Sur',
		      value: "Zambuanga del Sur"
		  }, 
		  {
		      name: 'Cagayan de Oro',
		      value: "Cagayan de Oro"
		  }];

		$scope.languagesfamily = [{
		      name: 'English',
		      value: "English"
		  }, {
		      name: 'Chinese',
		      value: "Chinese"
		  }, {
		      name: 'Pilipino',
		      value: "Pilipino"
		  }];

		$scope.languagesdialect = [{
		      name: 'Tagalog',
		      value: "Tagalog"
		  }, {
		      name: 'Ilocano',
		      value: "Ilocano"
		  }, {
		      name: 'Waray',
		      value: "Waray"
		  }, {
		      name: 'Ilongo',
		      value: "Ilongo"
		  }, {
		      name: 'Cebuano',
		      value: "Cebuano"
		  }];

		$scope.religions = [{
		      name: 'Roman Catholic',
		      value: "Roman Catholic"
		  }, {
		      name: 'Islam',
		      value: "Islam"
		  }, {
		      name: 'Protestants',
		      value: "Protestants"
		  }, {
		      name: 'Evangelical Christian',
		      value: "Evangelical Christian"
		  }, {
		      name: 'Igleisa ni Cristo',
		      value: "Igleisa ni Cristo"
		  }, {
		      name: 'Jehovahs witness',
		      value: "Jehovahs witness"
		  }
		  ];

		$scope.problems = [
		    'Garbage Disposal', 
		    'Drainage', 
		    'Water', 
		    'Health And Sanitation',
		    'Drug Addiction',
		    'Infrastructure',
		    'Political',
		    'Education',
		    'Safety and Security'
		];

		$scope.brgyjudicial = {
		      problems: []
		  };

		$scope.incomes = [
		      'Below P10K',
		      'P10K - P30K',
		      'P30K - P50K',
		      'More than P50K'
		  ];

		$scope.brgyjudicial = {
		      incomes: []
		  };

		$scope.cottages = [
		      'Food Preservation',
		      'Food Processing',
		      'Handicraft',
		      'Others',
		  ];

		$scope.brgyjudicial = {
		      cottages: []
		  };

		$scope.OEAs = [
		      'Sari - sari Store',
		      'Barbecue Stall',
		      'Meat Stall',
		      'Vegetable Stall',
		      'Fishing Vending',
		      'Others'
		  ];

		$scope.brgyjudicial = {
		            OEAs: []
		        };

   $http.get(DbCollection + 'account/')
      .then(function(result){
      $rootScope.UserAcount = result.data;
      });


      var currentmunicipal = $scope.UserAcount.municipality;
      console.log(currentmunicipal);


		  $scope.savejudcial = function(id) {

			$scope.brgyjudicial.minutes =	$scope.minutes;
			$scope.brgyjudicial.hours =	$scope.hours;

		
			$http.post(DbCollection+'/judicial', $scope.brgyjudicial)
			.then(function(result){
				$scope.result = result.data;
				$modalInstance.dismiss('cancel');
		          window.location.reload();

			},function (error) {
                        $scope.error = error.data.message;
                    });
			  $scope.brgyjudicial.respondent.case_type = $scope.brgyjudicial.case_type;
			$http.put(DbCollection + 'residentset/'+ id, $scope.brgyjudicial.respondent)
             .then(function(result){
              $scope.result2 = result.data;

             });

		  }
    }

});


