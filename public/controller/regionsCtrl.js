'use strict';

var app = angular.module('brgyapp');

app.controller('regionCtrl', function($scope, $modal, $http, $log, $rootScope, $timeout, $route, $q, Restangular, $location, $filter, ngTableParams, DbCollection) {
  
  var referrerId = $rootScope.UserAcount.createdbyId;

 $http.get(DbCollection + 'region/')
     .then(function(result){
       $scope.region = result.data


       return $scope.tableParams = new ngTableParams({

          page: 1,            // show first page
          count: 5           // count per page
      }, {
          total: $scope.region.length, // length of data
          getData: function($defer, params) {
              // use build-in angular filter
              var orderedData = params.sorting() ?
                      $filter('orderBy')($scope.region, params.orderBy()) :
                      $scope.region;

                 $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
      });
    });

  $scope.openmodal = function (size, id) {
 
    var modalInstance = $modal.open({
      templateUrl: '../views/region/region.html',
      controller: $scope.model,
      size: size,
      resolve: {
            getregion: function($http){
                if(id){
                  return $http.get(DbCollection + '/region/'+ id);
                }else{
                  return null;
                  
                }
              }
            }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
      }, function () {
        // $log.info('Modal dismissed at: ' + new Date());
      });

  };

    $scope.model = function($scope, $modalInstance, $modal, getregion, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {
    
    $rootScope.regionHeading = 'Region Form';
        
          $scope.ok = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

         $scope.save = function() {
             var referrerId = $rootScope.UserAcount.createdbyId;
             $scope.region.referrerId = referrerId;
             
             Restangular.all('region').post($scope.region).then(function(region) {
              $modalInstance.dismiss('cancel');
              window.location.reload();
            });
          }

          if(getregion){
            return $scope.region = getregion.data;
          }else{
            return null;
          }

  }
   
 });


app.controller('editregionCtrl', function($scope, $resource, $modal, $log, $rootScope, Region, $http, Restangular, $timeout, $route, $q, $location, $filter, ngTableParams, DbCollection) {

  	  $rootScope.regionHeading = 'Region Form'; 

    $scope.province_modal = function (size, id) {
   
      var modalInstance = $modal.open({
        templateUrl: '../views/region/province.html',
        controller: $scope.model,
        size: size,
        resolve: {
              Region: function($http){
                  if(id){
                    return $http.get(DbCollection + '/region/'+ id);
                  }else{
                    return null;
                    
                  }
                }
              }
     });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        }, function () {
          // $log.info('Modal dismissed at: ' + new Date());
        });

    };

    $scope.province_edit_modal = function (size, id) {

      var modalInstance = $modal.open({
        templateUrl: '../views/region/province.html',
        controller: $scope.editmodal,
        size: size,
        resolve: {
              Province: function($http){
                  if(id){
                    return $http.get(DbCollection + '/getprovince/'+ id)
                  }else{
                    return null;
                    
                  }
                }
              }
     });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        }, function () {
          // $log.info('Modal dismissed at: ' + new Date());
        });

    };

    $scope.model = function($scope, $modalInstance, $modal, Region, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {
    
    $rootScope.regionHeading = 'Province Form';
        
          $scope.ok = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };


          var referrerId = $rootScope.UserAcount.createdbyId;

          $scope.region = Region.data;

          $scope.saveprovince = function() {
            $scope.province.region = $scope.region.region_name;
            $scope.province.regionId = $scope.region._id;
            $http.post(DbCollection+'/province', $scope.province)
            .then(function(result){
              $scope.province = result.data;
              $modalInstance.dismiss('cancel');
              window.location.reload();
            });
         }


          if(Region){
            return $scope.getregion = Region.data;
          }else{
            return null;
          }

    }

    $scope.editmodal = function($scope, $modalInstance, $modal, Province, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {
   
    $scope.province = Province.data;
    $scope.regionHeading = 'Province Form';
        
          $scope.ok = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
        
          var referrerId = $rootScope.UserAcount.createdbyId;

          $scope.Update = function(id) {
            $http.put(DbCollection+'province/'+ id, $scope.province)
            .then(function(result){
              $scope.province = result.data;
              $modalInstance.dismiss('cancel');
              window.location.reload();
            });
         }


          if(Province){
            return $scope.province = Province.data;
          }else{
            return null;
          }

    }

  $scope.getregion = Region;

  $scope.region = Restangular.copy($scope.getregion);

    $http.get(DbCollection + 'province/')
     .then(function(result){
       $scope.provinces = result.data


       return $scope.tableParams = new ngTableParams({

          page: 1,            // show first page
          count: 5           // count per page

      }, {
          total: $scope.provinces.length, // length of data
          getData: function($defer, params) {
              // use build-in angular filter
              var orderedData = params.sorting() ?
                      $filter('orderBy')($scope.provinces, params.orderBy()) :
                      $scope.provinces;

                 $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
      });
    });

       $scope.show = function(id){
           $location.url('/viewmunicipality/' + id);
        };

});


