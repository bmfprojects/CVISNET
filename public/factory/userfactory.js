var app = angular.module('brgyapp');

// app.factory('Currentuser', function ($resource, $rootScope, DbCollection, $http){
    
//     function getuser(){
//         $http.get(DbCollection+'/account')
//         .then(function(result){
//             return result.data;
//         })
//     }
// });

app.factory("authenticationSvc", function($http, $q, $window, $timeout, loginCollection) {
    var userInfo;

    function login(userName, password) {
        var deferred = $q.defer();

        $http.post(loginCollection + "/login", { username: userName, password: password })
            .then(function (result) {
                userInfo = {
                    accessToken: result.data.access_token,
                    username: result.data.username,
                    expires: result.data.expires
                };
                $window.sessionStorage["userInfo"] = JSON.stringify(userInfo);
                deferred.resolve(userInfo);
                console.log(userInfo);
            }, function (error) {
                deferred.reject(error);
            });

        return deferred.promise;

    }

    function logout() {
        var deferred = $q.defer();

        $http({
            method: "POST",
            url: "/users/logout",
            headers: {
                "access_token": userInfo.accessToken
            }
        }).then(function (result) {
            userInfo = null;
            $window.sessionStorage["userInfo"] = null;
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });

        return deferred.promise;
    }

    function getUserInfo() {
        return userInfo;
    }

    function init() {
        if ($window.sessionStorage["userInfo"]) {
            userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
    }
    init();

    return {
        login: login,
        logout: logout,
        getUserInfo: getUserInfo
    };
});