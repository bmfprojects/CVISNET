var express 	= require('express'),
	config 		= require("./configs"),
	api 		= require('./api'),
	users 		= require('./account'),
	app 		= express();
	

app

.use(express.static('./public'))
.use(users)
.use('/api', api)
.use('/users', users)
.get('*', function (req, res){
		
		//if(sessionStorage == 0){
		//res.redirect('/logout');

	res.sendfile('public/index.html');
		
})

.listen(config.server.port);
 console.log(config.message);
 console.log('Database URL running "' + config.mongodb.brgydata + '..."');
 console.log("Server listening on port " + config.server.port);
