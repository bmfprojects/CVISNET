var express 		= require('express'),
	config 			= require("./configs"),
	bodyparser  	= require('body-parser'),
	mongodb 		= require('mongoskin'),
	cors 			= require('cors'),
	session 		= require('express-session'),
	db 				= mongodb.db(config.mongodb.brgydata, {native_parser:true}); 
	router 			= express.Router(),
	uuid            = require('node-uuid'),
	methodOverride 	= require('method-override');
	
//=========================================================================
//REGION COLLECTION
//=========================================================================

router
	.use(cors())
	.use(bodyparser.json({ limit: '10000mb'}))
	.use(bodyparser.urlencoded({ limit: '10000mb'}))
	.use(methodOverride())
	.route('/region')
	.get(function (req, res){
		db.collection('region').find().toArray(function (err, data){
			res.json(data);
		});
	})

   .post(function(req, res) {
	var region = req.body;
	region.usercurrentId = req.user;
    db.collection('region').insert(region, function (err, data){
			res.json(data);
		});
	});

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb'}))
    .use(bodyparser.json({limit: '10000mb'}))
    .use(methodOverride())
	.route('/region/:id')
	.get(function (req, res){
		db.collection('region').findById(req.params.id, function (err, data){
			res.json(data);
		});
	})
	.put(function (req, res, next){
		var region = req.body;
		delete region._id;
		db.collection('region').updateById(req.params.id, region, function (err, data){
			res.json(data);
		});
	})
	.delete(function (req, res){
		db.collection('region').removeById(req.params.id, function(){
			res.json(null);
		});
	});

//=========================================================================
//PROVINCE COLLECTION
//=========================================================================

router
	.use(cors())
	.use(bodyparser.json({limit: '10000mb'}))
	.use(bodyparser.urlencoded({limit: '10000mb'}))
	.use(methodOverride())
	.route('/province')
	.get(function (req, res){
		db.collection('province').find().toArray(function (err, data){
			res.json(data);
		});
	})

   .post(function(req, res) {
	var province = req.body;
	province.usercurrentId = req.user;
    db.collection('province').insert(province, function (err, data){
			res.json(data);
		});
	});
router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb'}))
    .use(bodyparser.json({limit: '10000mb'}))
    .use(methodOverride())
	.route('/getprovince/:id')
	.get(function (req, res){
		db.collection('province').findById(req.params.id, function (err, data){
			res.json(data);
		});
	});
router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb'}))
    .use(bodyparser.json({limit: '10000mb'}))
    .use(methodOverride())
	.route('/province/:id')
	.get(function (req, res){
		var region = {
			regionId : req.params.id
		}
		db.collection('province').find(region).toArray(function (err, data){
			res.json(data);
		});
	})
	.put(function (req, res, next){
		var province = req.body;
		delete province._id;
		db.collection('province').updateById(req.params.id, province, function (err, data){
			res.json(data);
		});
	})
	.delete(function (req, res){
		db.collection('province').removeById(req.params.id, function(){
			res.json(null);
		});
	});

//=========================================================================
//MUNICIPALITY COLLECTION
//=========================================================================

router
	.use(cors())
	.use(bodyparser.json({limit: '10000mb'}))
	.use(bodyparser.urlencoded({limit: '10000mb'}))
	.use(methodOverride())
	.route('/municipality')
	.get(function (req, res){
		db.collection('municipality').find().toArray(function (err, data){
			res.json(data);
		});
	})

   .post(function(req, res) {
	var municipality = req.body;
	municipality.usercurrentId = req.user;
    db.collection('municipality').insert(municipality, function (err, data){
			res.json(data);
		});
	})
router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb'}))
    .use(bodyparser.json({limit: '10000mb'}))
    .use(methodOverride())
	.route('/getmunicipality/:id')
	.get(function (req, res){
		db.collection('municipality').findById(req.params.id, function (err, data){
			res.json(data);
		});
	})

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb'}))
    .use(bodyparser.json({limit: '10000mb'}))
    .use(methodOverride())
	.route('/municipality/:id')
	.get(function (req, res){
		db.collection('municipality').find({provinceId : req.params.id}).toArray(function (err, data){
			res.json(data);
		});
	})
	.put(function (req, res, next){
		var municipality = req.body;
		delete municipality._id;
		db.collection('municipality').updateById(req.params.id, municipality, function (err, data){
			res.json(data);
		});
	})
	.delete(function (req, res){
		db.collection('municipality').removeById(req.params.id, function(){
			res.json(null);
		});
	});

//=========================================================================
//BARANGAY COLLECTION
//=========================================================================

router
	.use(cors())
	.use(bodyparser.json({limit: '10000mb'}))
	.use(bodyparser.urlencoded({limit: '10000mb'}))
	.use(methodOverride())
	.route('/brgycol')
	.get(function (req, res){
		db.collection('barangay').find().toArray(function (err, data){
			res.json(data);
		});
	})

   .post(function(req, res) {
	var brgydata = req.body;
	brgydata.usercurrentId = req.user;
    db.collection('barangay').insert(brgydata, function (err, data){
			res.json(data);
		});
	});

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb'}))
    .use(bodyparser.json({limit: '10000mb'}))
    .use(methodOverride())
	.route('/getbrgycol/:id')
	.get(function (req, res){
		var brgy = {
			municipalId : req.params.id
		}
		db.collection('barangay').findById(req.params.id, function (err, data){
			res.json(data);
		});
	}); 

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb'}))
    .use(bodyparser.json({limit: '10000mb'}))
    .use(methodOverride())
	.route('/brgycol/:id')
	.get(function (req, res){
		var brgy = {
			municipalId : req.params.id
		}
		db.collection('barangay').find(brgy).toArray(function (err, data){
			res.json(data);
		});
	})
	.put(function (req, res, next){
		var brgy = req.body;
		delete brgy._id;
		db.collection('barangay').updateById(req.params.id, brgy, function (err, data){
			res.json(data);
		});
	})
	.delete(function (req, res){
		db.collection('barangay').removeById(req.params.id, function(){
			res.json(null);
		});
	});

// ====================================================	
// DATA FILTERING FUNCTION
// ====================================================

function getuserrole (req, res, accesscontrol, colname){

	if(colname === 'brgyresident'){

		if(accesscontrol === 'barangayaccesscontrol'){

			db.collection(colname).find({ barangay: req.barangay}).toArray(function (err, data){
            return res.json(data);
			});

		}else if(accesscontrol === 'municipalitycontrol'){

			db.collection(colname).find({ municipality: req.municipality}).toArray(function (err, data){
			 	return res.json(data);
			});

		}else if(accesscontrol === 'regionadmin'){
		
			db.collection(colname).find().toArray(function (err, data){
			 	return res.json(data);
			});

		}else if(accesscontrol === 'user'){
		
			db.collection(colname).find({ barangay: req.barangay }).toArray(function (err, data){
			 	return res.json(data);
			});

		};

	}else if(colname === 'brgyjudicial'){

		if(accesscontrol === 'barangayaccesscontrol'){

			db.collection(colname).find({ usercurrentId: req.user}).toArray(function (err, data){
			 	return res.json(data);
			});

		}else if(accesscontrol === 'municipalitycontrol'){

			db.collection(colname).find({ referrermunicipality: req.municipality}).toArray(function (err, data){
			 	return res.json(data);
			});

		}else if(accesscontrol === 'regionadmin'){

			db.collection(colname).find().toArray(function (err, data){
			 	return res.json(data);
			});

		}else if(accesscontrol === 'user'){
		
			db.collection(colname).find({ referrerbarangay: req.barangay}).toArray(function (err, data){
			 	return res.json(data);
			});
		};

	}else if(colname === 'brgyclearance'){

		if(accesscontrol === 'barangayaccesscontrol'){

			db.collection(colname).find({ barangay: req.barangay}).toArray(function (err, data){
			 	return res.json(data);
			});

		}else if(accesscontrol === 'municipalitycontrol'){

			db.collection(colname).find({ municipality: req.municipality}).toArray(function (err, data){
			 	return res.json(data);
			});

		}else if(accesscontrol === 'regionadmin'){

			db.collection(colname).find().toArray(function (err, data){
			 	return res.json(data);
			});

		}else if(accesscontrol === 'user'){
		
			db.collection(colname).find({ barangay: req.barangay}).toArray(function (err, data){
			 	return res.json(data);
			});
		};
	};		
};

//=========================================================================
//CLEARANCE COLLECTION
//=========================================================================

router
	.use(cors())
	.use(bodyparser.json({limit: '10000mb'}))
	.use(bodyparser.urlencoded({limit: '10000mb'}))
	.use(methodOverride())
	.route('/clearance')
	.get(function (req, res){

		var clearance = 'brgyclearance';
		var role = req.accesscontrol
		getuserrole(req, res, role, clearance);

	})

   .post(function(req, res) {


 		var inno_id = uuid.v4();
 		var today = new Date();

		var clearance = req.body;
		    clearance.residentId 			= clearance.resident._id;
		    clearance.clearance_control_no 	= inno_id;
            clearance.region 				= req.region;
            clearance.province 				= req.province;
            clearance.municipality 			= req.municipality;
            clearance.barangay 				= req.barangay;
            clearance.createdby 			= req.byusername;
			clearance.usercurrentId 		= req.user;
			clearance.date_of_issue 		= today;

			var purpose = req.body.purpose;
			var remark = req.body.remark;
			var jud_issuedON = req.body.jud_issuedON;
			var OR = req.body.OR;

			if(purpose == null){
				res.send(301, {
							message: 'Purpose field is empty.'});
			}else if(jud_issuedON == null){
				res.send(301, {
							message: 'Issued On field is empty.'});
			}else if(remark == null){
				res.send(301, {
							message: 'Remark field is empty.'});
			}else if(OR == null){
				res.send(301, {
							message: 'OR Number field is empty.'});
			}else{
				    db.collection('brgyclearance').insert(clearance, function (err, data){
						res.json(data);
					});
			}

	});
router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb'}))
    .use(bodyparser.json({limit: '10000mb'}))
    .use(methodOverride())
	.route('/getclearance/:id')
	.get(function (req, res){
	
		db.collection('brgyclearance').findById(req.params.id, function (err, data){
		    res.json(data);
		});
		
	})
router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb'}))
    .use(bodyparser.json({limit: '10000mb'}))
    .use(methodOverride())
	.route('/clearance/:id')
	.get(function (req, res){
		db.collection('brgyclearance').find({residentId:req.params.id}).toArray(function (err, data){
			res.json(data);
		});

	})
	.put(function (req, res, next){

		var inno_id = uuid.v4();
 		var today = new Date();

		var clearance = req.body;
		    clearance.residentId 			= clearance.resident._id;
		    clearance.clearance_control_no 	= inno_id;
            clearance.region 				= req.region;
            clearance.province 				= req.province;
            clearance.municipality 			= req.municipality;
            clearance.barangay 				= req.barangay;
            clearance.createdby 			= req.byusername;
			clearance.usercurrentId 		= req.user;
			clearance.date_of_issue 		= today;

			var purpose = req.body.purpose;
			var remark = req.body.remark;
			var jud_issuedON = req.body.jud_issuedON;
			var OR = req.body.OR;

			if(purpose == null){
				res.send(301, {
							message: 'Purpose field is empty.'});
			}else if(jud_issuedON == null){
				res.send(301, {
							message: 'Issued On field is empty.'});
			}else if(remark == null){
				res.send(301, {
							message: 'Remark field is empty.'});
			}else if(OR == null){
				res.send(301, {
							message: 'OR Number field is empty.'});
			}else{

				delete clearance._id;
				db.collection('brgyclearance').updateById(req.params.id, clearance, function (err, data){
					res.json(data);
				});
			}
		

		
		
	})
	.delete(function (req, res){
		db.collection('brgyclearance').removeById(req.params.id, function(){
			res.json(null);
		});
	});

//=========================================================================
//JUDICIAL COLLECTION
//=========================================================================
router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb'}))
	.use(bodyparser.json({limit: '10000mb'}))
	.use(methodOverride())
	.route('/judicial')
	.get(function (req, res){

		var judicial = 'brgyjudicial';
		var role = req.accesscontrol
		getuserrole(req, res, role, judicial);

	})
   .post(function(req, res, next) {

	  var brgyjudicial = req.body;

		  brgyjudicial.usercurrentId = req.user;
		  brgyjudicial.referrerId =	req.currentIdref;
		  brgyjudicial.referrerprovince = req.province;
		  brgyjudicial.referrerregion = req.region ;
		  brgyjudicial.referrerbarangay = req.barangay;
		  brgyjudicial.referrermunicipality = req.municipality;

		  var brgyresident 			= req.body;
				var respondent 				= req.body.respondent;
				var complainantresident 	= req.body.complainant_resident;
				var complainanta 			= req.body.complainant;
				var respondentresident 		= req.body.respondent_resident;
				var casetypea				= req.body.case_type;
				var datee 					= req.body.prof_date;

				db.collection('brgyjudicial').find({

					respondent : req.body.respondent,
					case_type: req.body.case_type,

				}).toArray(function (err,data) {

					if(respondent == null){
						res.send(301, {
							message: 'Respondent field is empty.'});

					}else if(complainantresident == null){
						res.send(301, {
							message: 'Is the respondent a resident of the barangay?'});

					}else if(complainanta == null){
						res.send(301, {
							message: 'Complainant field is empty,'});

					}else if(respondentresident == null){
						res.send(301, {
							message: 'Is the complainant a resident of the barangay?'});

					}else if(casetypea == null){
						res.send(301, {
							message: 'Case type field is empty.'});

					}else if(datee == null){
						res.send(301, {
							message: 'Please select date.'});

					}else{

						db.collection('brgyjudicial').insert(brgyresident, function(err, data){
							  return res.json(data);
						});
					}
				});
	
});


router
	.use(cors())
	.use(bodyparser.urlencoded({limit: '10000mb'}))
    .use(bodyparser.json({limit: '10000mb'}))
    .use(methodOverride())
	.route('/judicial/:id')
	.get(function (req, res){
		db.collection('brgyjudicial').findById(req.params.id, function (err, data){
			res.json(data);
		}) 
	})
	.put(function (req, res, next){

		var judicial = req.body;

				var respondent 				= req.body.respondent;
				var complainantresident 	= req.body.complainant_resident;
				var complainanta 			= req.body.complainant;
				var respondentresident 		= req.body.respondent_resident;
				var casetypea				= req.body.case_type;
				var datee 					= req.body.prof_date;

		db.collection('brgyjudicial').find({

					respondent : req.body.respondent,
					case_type: req.body.case_type,

				}).toArray(function (err,data) {

					if(respondent == null){
						res.send(301, {
							message: 'Respondent field is empty.'});

					}else if(complainantresident == null){
						res.send(301, {
							message: 'Is the respondent a resident of the barangay?'});

					}else if(complainanta == null){
						res.send(301, {
							message: 'Complainant field is empty,'});

					}else if(respondentresident == null){
						res.send(301, {
							message: 'Is the complainant a resident of the barangay?'});

					}else if(casetypea == null){
						res.send(301, {
							message: 'Case type field is empty.'});

					}else if(datee == null){
						res.send(301, {
							message: 'Please select date.'});

					}else{

						delete judicial._id;

							db.collection('brgyjudicial').updateById(req.params.id, judicial, function (err, data){
								res.json(data);
							});
					}
				});


	})
	.delete(function (req, res){
		db.collection('brgyjudicial').removeById(req.params.id, function(){
			res.json(null);
		});
	});

//=========================================================================
//RESIDENT COLLECTION
//=========================================================================

function postdata (req, res, colname, accesscontrol){

		if(accesscontrol === 'barangayaccesscontrol'){
			var brgyresident 			= req.body;
			brgyresident.usercurrentId 	= req.user;
			brgyresident.barangay 		= req.barangay;
			brgyresident.municipality 	= req.municipality;
			brgyresident.region 		= req.region;
			brgyresident.province 		= req.province;
			brgyresident.judicial 		= 'false';
			brgyresident.createdby 		= req.byusername; 
			brgyresident.captain 		= req.captain;
			brgyresident.sk_chairman 	= req.sk_chairman;
			brgyresident.kagawad 		= req.kagawad; 
			brgyresident.treasurer 		= req.treasurer;
			brgyresident.secretary		= req.secretary;

			var fname 				= req.body.prof_firstname,
				lname 				= req.body.prof_lastname,
				mname 				= req.body.prof_middle_name,
				region_bplace 		= req.body.region_bplace,
				province_bplace 	= req.body.province_bplace,
				municipality_bplace = req.body.municipality_bplace,
				barangay_bplace 	= req.body.barangay_bplace,
				imageuri 			= req.body.imageuri,
				prof_status 		= req.body.prof_status,
				prof_gender 		= req.body.prof_gender,
				b_date 				= req.body.b_date;

			db.collection(colname).find({

				prof_firstname : req.body.prof_firstname, 
				prof_lastname: req.body.prof_lastname, 
				prof_middle_name: req.body.prof_middle_name

			}).toArray(function (err, data){
					if(data.length){
						res.send(301, {
						
						message: 'Sorry, cannot save "'+
						fname +' '+ 
						mname +' '+ 
						lname +'". Data already exist with different records.' });
						
					}else if(fname == null){
						res.send(301, {
						message: 'First name is empty.'});
						
					}else if(mname == null){
						res.send(301, {
						message: 'Middle name is empty.'});
						
					}else if(lname == null){
						res.send(301, {
						message: 'Last name is empty.'});
						
					}else if(region_bplace == null){
						res.send(301, {
						message: 'Region is empty.'});
						
					}else if(province_bplace == null){
						res.send(301, {
						message: 'Province is empty.'});
						
					}else if(municipality_bplace == null){
						res.send(301, {
						message: 'Municipality is empty.'});
						
					}else if(barangay_bplace == null){
						res.send(301, {
						message: 'Barangay is empty.'});
						
					}else if(prof_status == null){
						res.send(301, {
						message: 'Please Select the status.'});
						
					}else if(prof_gender == null){
						res.send(301, {
						message: 'Please Select the gender.'});
						
					}else if(imageuri == null){
						res.send(301, {
						message: 'Please upload profile picture.'});
						
					}else if(b_date == null){
						res.send(301, {
						message: 'Birth Date is empty.'});
						
					}else{

						db.collection(colname).insert(brgyresident, function(err, data){
						  return res.json(data);
						});
					}
				});

		}else if(accesscontrol === 'municipalitycontrol'){

			var brgyresident = req.body;
			brgyresident.usercurrentId = req.user;
			brgyresident.municipality = req.municipality;
			brgyresident.region = req.region;
			brgyresident.province = req.province;
			brgyresident.judicial = 'false';
			brgyresident.createdby = req.byusername; 

		    db.collection(colname).insert(brgyresident, function (err, data){
				return res.json(data);
			});

		}else if(accesscontrol === 'regionadmin'){
			var brgyresident = req.body;
			brgyresident.usercurrentId = req.user;
			brgyresident.judicial = 'false';
			brgyresident.createdby = req.byusername; 

			var fname = req.body.prof_firstname,
				lname = req.body.prof_lastname,
				mname = req.body.prof_middle_name

			db.collection('brgyresident').count(
			{
				prof_firstname : req.body.prof_firstname, 
				prof_lastname: req.body.prof_lastname, 
				prof_middle_name: req.body.prof_middle_name
			}, 
			function (err, count) {
				if (count !== 0) {
					res.send(301, {

						message: 'Sorry, cannot save '+
						fname +' '+ 
						mname +' '+ 
						lname +'. Data already exist with different records.' });

				} else {
				    db.collection('brgyresident').insert(brgyresident, function (err, data){
					res.json(data);
					});
				}
			})

		}
};

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '1000mb'}))
	.use(bodyparser.json({limit: '1000mb'}))
	.use(methodOverride())
	.route('/judicial_resident')
	.get(function (req, res){

			db.collection('brgyresident').find().toArray(function (err, data){
            return res.json(data);
			});
			
		

	});

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb'}))
	.use(bodyparser.json({limit: '10000mb'}))
	.use(methodOverride())
	.route('/resident')
	.get(function (req, res){
		var resident = 'brgyresident';
		var role = req.accesscontrol
		getuserrole(req, res, role, resident);
			
		

	})
   .post(function(req, res) {
   	var resident = 'brgyresident', accesscontrol = req.accesscontrol
		postdata(req, res, resident, accesscontrol);
	});
   
router
	.use(cors())
	.use(bodyparser.urlencoded({limit: '10000mb'}))
    .use(bodyparser.json({limit: '10000mb'}))
    .use(methodOverride())
	.route('/residentset/:id')
	.put(function (req, res){

	var brgyresident = req.body;
		brgyresident.judicial = 'true';
		brgyresident.badrecord = 'Yes';
		brgyresident.createdby 		= req.byusername; 
		brgyresident.captain 		= req.captain;
		brgyresident.sk_chairman 	= req.sk_chairman;
		brgyresident.kagawad 		= req.kagawad; 
		brgyresident.treasurer 		= req.treasurer;
		brgyresident.secretary		= req.secretary;

	    delete brgyresident._id;

			db.collection('brgyresident').updateById(req.params.id, brgyresident, function(err, data) {
				   res.json(data);
			});
	
	});
router
	.use(cors())
	.use(bodyparser.urlencoded({limit: '10000mb'}))
    .use(bodyparser.json({limit: '10000mb'}))
    .use(methodOverride())
	.route('/get_qr_resident/:id')
	.get(function (req, res){
		db.collection('brgyresident').findById(req.params.id, function (err, data){
			
			if(data == null){
			
				res.send(301, {
								message: 'Qr value not found!'});
			}else{


			var getage = calculate_age(data.b_date);
			//calculate the age base on the birthdate given.
            function calculate_age(dateString){
                  var birthday = new Date(dateString);
                  var ageDifMs = Date.now() - birthday.getTime();
                  var ageDate = new Date(ageDifMs); 
                  return Math.abs(ageDate.getFullYear() - 1970);

            };

            data.current_age = getage;
            res.json(data);
			}



		})
	})

router
	.use(cors())
	.use(bodyparser.urlencoded({limit: '10000mb'}))
    .use(bodyparser.json({limit: '10000mb'}))
    .use(methodOverride())
	.route('/resident/:id')
	.get(function (req, res){
		db.collection('brgyresident').findById(req.params.id, function (err, data){
			
			var getage = calculate_age(data.b_date);
			// calculate the age base on the birthdate given.
            function calculate_age(dateString){
                  var birthday = new Date(dateString);
                  var ageDifMs = Date.now() - birthday.getTime();
                  var ageDate = new Date(ageDifMs); // miliseconds from epoch
                  return Math.abs(ageDate.getFullYear() - 1970);

            };

            data.current_age = getage;
            res.json(data);
		})
	})
	.put(function (req, res){

			var brgyresident 			= req.body;
			brgyresident.usercurrentId 	= req.user;
			brgyresident.barangay 		= req.barangay;
			brgyresident.municipality 	= req.municipality;
			brgyresident.region 		= req.region;
			brgyresident.province 		= req.province;
			brgyresident.createdby 		= req.byusername; 
			brgyresident.captain 		= req.captain;
			brgyresident.sk_chairman 	= req.sk_chairman;
			brgyresident.kagawad 		= req.kagawad; 
			brgyresident.treasurer 		= req.treasurer;
			brgyresident.secretary		= req.secretary;

			var fname 				= req.body.prof_firstname,
				lname 				= req.body.prof_lastname,
				mname 				= req.body.prof_middle_name,
				region_bplace 		= req.body.region_bplace,
				province_bplace 	= req.body.province_bplace,
				municipality_bplace = req.body.municipality_bplace,
				barangay_bplace 	= req.body.barangay_bplace,
				imageuri 			= req.body.imageuri,
				prof_status 		= req.body.prof_status,
				prof_gender 		= req.body.prof_gender,
				b_date 				= req.body.b_date;

			db.collection('brgyresident').find({

				prof_firstname : req.body.prof_firstname, 
				prof_lastname: req.body.prof_lastname, 
				prof_middle_name: req.body.prof_middle_name

			}).toArray(function (err, data){

					if(data.length){

						if(data[0]._id == req.body._id){

							if(fname == null){
								res.send(301, {
								message: 'First name is empty.'});
								
							}else if(mname == null){
								res.send(301, {
								message: 'Middle name is empty.'});
								
							}else if(lname == null){
								res.send(301, {
								message: 'Last name is empty.'});
								
							}else if(region_bplace == null){
								res.send(301, {
								message: 'Region is empty.'});
								
							}else if(province_bplace == null){
								res.send(301, {
								message: 'Province is empty.'});
								
							}else if(municipality_bplace == null){
								res.send(301, {
								message: 'Municipality is empty.'});
								
							}else if(barangay_bplace == null){
								res.send(301, {
								message: 'Barangay is empty.'});
								
							}else if(prof_status == null){
								res.send(301, {
								message: 'Please Select the status.'});
								
							}else if(prof_gender == null){
								res.send(301, {
								message: 'Please Select the gender.'});
								
							}else if(imageuri == null){
								res.send(301, {
								message: 'Please upload profile picture.'});
								
							}else if(b_date == null){
								res.send(301, {
								message: 'Birth Date is empty.'});
								
							}else{
								delete brgyresident._id;
								db.collection('brgyresident').updateById(req.params.id, brgyresident, function (err, data){
								  return res.json(data);
								});
							}

						}else if(data[0]._id !== req.body._id){

							res.send(301, {
						
								message: 'Sorry, cannot save "'+
								fname +' '+ 
								mname +' '+ 
								lname +'". Data already exist with different records.' });
						}

						
					}else if(!data.length){

						if(fname == null){
							res.send(301, {
							message: 'First name is empty.'});
							
						}else if(mname == null){
							res.send(301, {
							message: 'Middle name is empty.'});
							
						}else if(lname == null){
							res.send(301, {
							message: 'Last name is empty.'});
							
						}else if(region_bplace == null){
							res.send(301, {
							message: 'Region is empty.'});
							
						}else if(province_bplace == null){
							res.send(301, {
							message: 'Province is empty.'});
							
						}else if(municipality_bplace == null){
							res.send(301, {
							message: 'Municipality is empty.'});
							
						}else if(barangay_bplace == null){
							res.send(301, {
							message: 'Barangay is empty.'});
							
						}else if(prof_status == null){
							res.send(301, {
							message: 'Please Select the status.'});
							
						}else if(prof_gender == null){
							res.send(301, {
							message: 'Please Select the gender.'});
							
						}else if(imageuri == null){
							res.send(301, {
							message: 'Please upload profile picture.'});
							
						}else if(b_date == null){
							res.send(301, {
							message: 'Birth Date is empty.'});
							
						}else{
							
							delete brgyresident._id;
							db.collection('brgyresident').updateById(req.params.id, brgyresident, function (err, data){
							  return res.json(data);
							});
						}

					}
				});

	
	})
	.delete(function (req, res){
		db.collection('brgyresident').removeById(req.params.id, function(){
			res.json(null);
		});
	});


//=========================================================================
//MANAGE ACCOUNT COLLECTION
//=========================================================================


var crypto = require('crypto');

function hash (password) {
	return crypto.createHash('sha256').update(password).digest('hex');
}

router
	.use(cors())
	.use(bodyparser.json({limit: '10000mb'}))
	.use(bodyparser.urlencoded({limit: '10000mb'}))
	.use(methodOverride())
	.route('/registered')
	.get(function (req, res){
		db.collection('userlist').find({ createdbyId: req.user }).toArray(function (err, data){
			res.json(data);
		});
	})

   .post(function(req, res) {

    if(req.accesscontrol === 'regionadmin'){

      	var user 			= req.body;
		
		user.createdbyId 	= req.user;
		user.createdbyadmin	= req.byusername;
		user.role 			= 'admin';
		user.active_caption = 'Deactivate';
		user.accesscontrol 	= 'municipalitycontrol'
		user.active 		= 'Active';
	
			var municipality = req.body.municipality,
				region = req.body.region,
				themes = req.body.themes,
				province = req.body.province,
				username = req.body.username,
				password = req.body.pswrd,
				verifypassword = req.body.verifypassword

			if(region == null){
				res.send(301, {
						message: 'Please Select the Region.'});
			}else if(province == null){
				res.send(301, {
						message: 'Please Select the Province.'});
			}else if(themes == null){
				res.send(301, {
						message: 'Please Select the Themes.'});
			}else if(municipality == null){
				res.send(301, {
						message: 'Please Select the Municipality.'});
			}else if(username == null){
				res.send(301, {
						message: 'Error: Username is empty.'});
			}else if(password == null){
				res.send(301, {
						message: 'Error: Password is empty.'});
			}else if(verifypassword == null){
				res.send(301, {
						message: 'Error: Please verify your password.'});
			}else{

				db.collection('userlist').count(
				{
					region : region, 
					province : province, 
					municipality : municipality
				},
				function (err, data) {

					if (data !== 0) {
	
						res.send(301, {
							message: 'Error: For '+
							 region.region_name +', '+ 
							 province.province_name +', '+ 
							 municipality.municipality_name +' '+ 
							' has already been assigned.' });

					 } else {

						db.collection('userlist').find({username: username}).toArray(function (err, data){
							if(data.length){
										res.send(301, {
											message: 
											'Error: Sorry, cannot create account, username "'+ 
											username +'" exist!'});
								
							}else if(password !== verifypassword){
								
										res.send(301, {
											message: 
											'Error: Password not match!'});
							}else{
								user.password 	= hash(req.body.pswrd);
								delete user.pswrd;
								delete user.verifypassword;

								db.collection('userlist').insert(user, function(err, data){
								res.json(data);
								});
							}
						});
					}
				})

			}

    }else if (req.accesscontrol === 'municipalitycontrol'){

    	var user 			= req.body;
		
		user.createdbyId 	= req.user;
		user.municipality 	= req.municipality;
		user.region 		= req.region;
		user.province 		= req.province ;
		user.createdbyadmin	= req.byusername;	
		user.role 			= 'admin';
		user.active_caption = 'Deactivate';
		user.accesscontrol 	= 'barangayaccesscontrol'
		user.active 		= 'Active';
	
			var barangay = req.body.barangay,
				username = req.body.username,
				captain = req.body.captain,
				themes = req.body.themes,
				password = req.body.pswrd,
				captain = req.body.captain,
				verifypassword = req.body.verifypassword

			if(barangay == null){
				res.send(301, {
						message: 'Error: Please Select the Barangay.'});
			}else if(username == null){
				res.send(301, {
						message: 'Error: Username is empty.'});
			}else if(password == null){
				res.send(301, {
						message: 'Error: Password is empty.'});
			}else if(verifypassword == null){
				res.send(301, {
						message: 'Error: Please verify your password.'});
			}else if(password !== verifypassword){
				res.send(301, {
						message: 'Error: Password not match.'});
			}else if(themes == null){
				res.send(301, {
						message: 'Error: Please Select the themes.'});
			}else if(captain == null){
						res.send(301, {
								message: 'Error: Barangay Captain is empty.'});
					 }else{

				db.collection('userlist').count(
				{
					region 			: req.region, 
					province 		: req.province, 
					municipality 	: req.municipality,
					barangay 		: barangay
				},
				function (err, data) {

					if (data !== 0) {
	
						res.send(301, {
							message: 'Error: For '+
							 barangay.barangay_name +', '+ 
							 req.municipality.municipality_name +', '+ 
							 req.province.province_name +', From Region -> '+
							 req.region.region_name +', '+ 
							' has already been assigned!' });

					 }else {

						db.collection('userlist').find({username: username}).toArray(function (err, data){
							if(data.length){
										res.send(301, {
											message: 
											'Error: Sorry, cannot create account, username "'+ 
											username +'" exist!'});
								
							}else if(password !== verifypassword){
								
										res.send(301, {
											message: 
											'Error: Password not match!'});
							}else{
								user.password 		= hash(req.body.pswrd);
								delete user.pswrd;
								delete user.verifypassword;

								db.collection('userlist').insert(user, function(err, data){
								res.json(data);
								});
							}
						});
					}
				})

			}
    	}else if (req.accesscontrol === 'barangayaccesscontrol'){

    	var user 			= req.body;
		
		user.createdbyId 	= req.user;
		user.municipality 	= req.municipality;
		user.region 		= req.region;
		user.province 		= req.province ;
		user.barangay 		= req.barangay ;
		user.createdbyadmin	= req.byusername;	
		user.role 			= 'guest';
		user.active_caption = 'Deactivate';
		user.accesscontrol 	= 'user'
		user.active 		= 'Active';
	
			var barangay = req.body.barangay,
				username = req.body.username,
				themes = req.body.themes,
				password = req.body.pswrd,
				verifypassword = req.body.verifypassword

				db.collection('userlist').find({username: username}).toArray(function (err, data){
					if(data.length){
								res.send(301, {
									message: 
									'Error: Sorry, cannot create account, username "'+ 
									username +'" exist!'});
						
					}else if(password !== verifypassword){
						
								res.send(301, {
									message: 
									'Error: Password not match!'});
					}else if(themes == null){
						
								res.send(301, {
									message: 
									'Error: Please select the themes.'});
					}else{
						user.password 		= hash(req.body.pswrd);
						delete user.pswrd;
						delete user.verifypassword;

						db.collection('userlist').insert(user, function(err, data){
						res.json(data);
						});
					}
				})

    	}
	
});

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb'}))
    .use(bodyparser.json({limit: '10000mb'}))
    .use(methodOverride())
	.route('/account_deactivate/:id')
	.put(function (req, res){

		var user = req.body;
		
			delete user._id;

			if(user.pswrd){
				delete user.pswrd 
				delete user.verifypassword 
			}

			db.collection('userlist').updateById(req.params.id, user, function(err, data){
				res.json(data);
			});

	});

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb'}))
    .use(bodyparser.json({limit: '10000mb'}))
    .use(methodOverride())
	.route('/edit_own_account/:id')
	.put(function (req, res, next){

	function getmy_access(req, res){

 	    var user = req.body;

		var username = req.body.username,
			newpassword = req.body.pswrd,
			themes = req.body.themes,
			verifypassword = req.body.verifypassword

			if(newpassword){

				if(newpassword !== verifypassword){
			
					res.send(301, {
						message: 
						'Error: Password not match!'});
				}else if(username == null){
					res.send(301, {
									message: 
									'Error: Username is empty.'});
				}else{
						
						var i = 0;
					
						 db.collection('userlist').find({username: user.username}).toArray(function (err, data){
								
								
								if(data.length){

									if(data[0]._id == req.body._id){

										//console.log('username not change but password change')
										user.password = hash(req.body.pswrd);
										delete user._id;
										delete user.pswrd 
										delete user.verifypassword 
										db.collection('userlist').updateById(req.params.id, user, function(err, data){
										res.json(data);
										});

									}else if(data[0]._id !== req.body._id){

										//console.log('username is change but password change')
										res.send(301, {
												message: 
												'Error: Sorry, cannot create account "'+ 
												username +'" already taken.'});
									}

										
								}else if(themes == null){
									res.send(301, {
													message: 
													'Error: Please select the themes.'});
								}else{

									//console.log('data is change without conflict but password change');

									user.password = hash(req.body.pswrd);
									delete user._id;
									delete user.pswrd 
									delete user.verifypassword 

									db.collection('userlist').updateById(req.params.id, user, function(err, data){
									res.json(data);
									});	

								}

						});
					
				}
			}else if(themes == null){
				res.send(301, {
								message: 
								'Error: Please select the themes.'});
			}else if(username == null){
				res.send(301, {
								message: 
								'Error: Username is empty.'});
			}else{
					
				db.collection('userlist').find({username: user.username}).toArray(function (err, data){
								
						if(data.length){

							if(data[0]._id == req.body._id){

								//console.log('username not change and without change password')
						
								delete user._id;
						
								db.collection('userlist').updateById(req.params.id, user, function(err, data){
								res.json(data);
								});

							}else if(data[0]._id !== req.body._id){

								//console.log('username is change and without change password')
								res.send(301, {
										message: 
										'Error: Sorry, cannot create account "'+ 
										username +'" already taken.'});
							}

								
						}else{

							//console.log('data is change without conflict and without change password');

							delete user._id;
					
							db.collection('userlist').updateById(req.params.id, user, function(err, data){
							res.json(data);
							});	

						}

				});
						
			}
	
		}

		getmy_access(req, res);
	});

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb'}))
    .use(bodyparser.json({limit: '10000mb'}))
    .use(methodOverride())
	.route('/municipalityaccount/:id')
	.put(function (req, res, next){

		 	      	var user 				= req.body;
						user.updateby		= req.byusername;


					var municipality = req.body.municipality,
						username = req.body.username,
						newpassword = req.body.pswrd,
						themes = req.body.themes,
						verifypassword = req.body.verifypassword

								if(newpassword){

									if(newpassword !== verifypassword){
								
										res.send(301, {
											message: 
											"Error: Password not match."});

									}else if(username == null){
										res.send(301, {
											message: 
											"Error: username is empty."});
									}else if(themes == null){
										res.send(301, {
											message: 
											"Error: please select themes."});
									}else{

										 db.collection('userlist').find({username: user.username}).toArray(function (err, data){
												
												
												if(data.length){

													if(data[0]._id == req.body._id){

														//console.log('username not change but password change')
														user.password = hash(req.body.pswrd);
														delete user._id;
														delete user.pswrd 
														delete user.verifypassword 
														db.collection('userlist').updateById(req.params.id, user, function(err, data){
														res.json(data);
														});

													}else if(data[0]._id !== req.body._id){

														//console.log('username is change but password change')
														res.send(301, {
																message: 
																'Error: Sorry, cannot create account "'+ 
																username +'" already taken.'});
													}

														
												}else{

													//console.log('data is change without conflict but password change');

													user.password = hash(req.body.pswrd);
													delete user._id;
													delete user.pswrd 
													delete user.verifypassword 

													db.collection('userlist').updateById(req.params.id, user, function(err, data){
													res.json(data);
													});	

												}

										});
										
									}
								}else if(username == null){
									res.send(301, {
										message: 
										"Error: username is empty."});
								}else if(themes == null){
									res.send(301, {
										message: 
										"Error: please select themes."});
								}else{
										
									db.collection('userlist').find({username: user.username}).toArray(function (err, data){
													
											if(data.length){

												if(data[0]._id == req.body._id){

													//console.log('username not change and without change password')
											
													delete user._id;
											
													db.collection('userlist').updateById(req.params.id, user, function(err, data){
													res.json(data);
													});

												}else if(data[0]._id !== req.body._id){

													//console.log('username is change and without change password')
													res.send(301, {
															message: 
															'Error: Sorry, cannot create account "'+ 
															username +'" already taken.'});
												}

													
											}else{

												//console.log('data is change without conflict and without change password');

												delete user._id;
										
												db.collection('userlist').updateById(req.params.id, user, function(err, data){
												res.json(data);
												});	

											}

									});
											
								}
	});

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb'}))
    .use(bodyparser.json({limit: '10000mb'}))
    .use(methodOverride())
	.route('/registered/:id')
	.get(function (req, res){
		db.collection('userlist').findById(req.params.id, function (err, data){
			res.json(data);
		});
	})
	.put(function (req, res, next){

		 if(req.accesscontrol === 'regionadmin'){

		 	      	var user 				= req.body;
						user.updateby		= req.byusername;


					var municipality = req.body.municipality,
						username = req.body.username,
						newpassword = req.body.pswrd,
						themes = req.body.themes,
						verifypassword = req.body.verifypassword

								if(newpassword){

									if(newpassword !== verifypassword){
								
										res.send(301, {
											message: 
											"Error: Password not match."});

									}else if(username == null){
										res.send(301, {
											message: 
											"Error: username is empty."});
									}else if(themes == null){
										res.send(301, {
											message: 
											"Error: please select themes."});
									}else{

										 db.collection('userlist').find({username: user.username}).toArray(function (err, data){
												
												
												if(data.length){

													if(data[0]._id == req.body._id){

														//console.log('username not change but password change')
														user.password = hash(req.body.pswrd);
														delete user._id;
														delete user.pswrd 
														delete user.verifypassword 
														db.collection('userlist').updateById(req.params.id, user, function(err, data){
														res.json(data);
														});

													}else if(data[0]._id !== req.body._id){

														//console.log('username is change but password change')
														res.send(301, {
																message: 
																'Error: Sorry, cannot create account "'+ 
																username +'" already taken.'});
													}

														
												}else{

													//console.log('data is change without conflict but password change');

													user.password = hash(req.body.pswrd);
													delete user._id;
													delete user.pswrd 
													delete user.verifypassword 

													db.collection('userlist').updateById(req.params.id, user, function(err, data){
													res.json(data);
													});	

												}

										});
										
									}
								}else if(username == null){
									res.send(301, {
										message: 
										"Error: username is empty."});
								}else if(themes == null){
									res.send(301, {
										message: 
										"Error: please select themes."});
								}else{
										
									db.collection('userlist').find({username: user.username}).toArray(function (err, data){
													
											if(data.length){

												if(data[0]._id == req.body._id){

													//console.log('username not change and without change password')
											
													delete user._id;
											
													db.collection('userlist').updateById(req.params.id, user, function(err, data){
													res.json(data);
													});

												}else if(data[0]._id !== req.body._id){

													//console.log('username is change and without change password')
													res.send(301, {
															message: 
															'Error: Sorry, cannot create account "'+ 
															username +'" already taken.'});
												}

													
											}else{

												//console.log('data is change without conflict and without change password');

												delete user._id;
										
												db.collection('userlist').updateById(req.params.id, user, function(err, data){
												res.json(data);
												});	

											}

									});
											
								}
				
			}else if(req.accesscontrol === 'municipalitycontrol' || req.accesscontrol === 'barangayaccesscontrol'){


					var user 				= req.body;
						user.updateby		= req.byusername;


					var barangay = req.body.barangay,
						username = req.body.username,
						captain = req.body.captain,
						themes = req.body.themes,
						newpassword = req.body.pswrd,
						verifypassword = req.body.verifypassword

								if(newpassword){

									if(newpassword !== verifypassword){
								
										res.send(301, {
											message: 
											"Error: Password not match!"});
									}else if(captain == null){
										res.send(301, {
												message: 'Error: Baranggay Captain is empty!'});
									}else if(username == null){
										res.send(301, {
												message: 'Error: Username is empty.'});
									}else if(themes == null){
											res.send(301, {
													message: 'Error: Please Select themes.'});
									}else{
											
											 db.collection('userlist').find({username: user.username}).toArray(function (err, data){
													
													
													if(data.length){

														if(data[0]._id == req.body._id){

															//console.log('username not change but password change')
															user.password = hash(req.body.pswrd);
															delete user._id;
															delete user.pswrd 
															delete user.verifypassword 
															db.collection('userlist').updateById(req.params.id, user, function(err, data){
															res.json(data);
															});

														}else if(data[0]._id !== req.body._id){

															//console.log('username is change but password change')
															res.send(301, {
																	message: 
																	'Error: Sorry, cannot create account "'+ 
																	username +'" already taken.'});
														}

															
													}else{

														//console.log('data is change without conflict but password change');

														user.password = hash(req.body.pswrd);
														delete user._id;
														delete user.pswrd 
														delete user.verifypassword 

														db.collection('userlist').updateById(req.params.id, user, function(err, data){
														res.json(data);
														});	

													}

											});
										
									}

								}else if(captain == null){
										res.send(301, {
												message: 'Error: Baranggay Captain is empty.'});
								}else if(username == null){
										res.send(301, {
												message: 'Error: Username is empty.'});
								}else if(themes == null){
										res.send(301, {
												message: 'Error: Please Select themes.'});
								}else{
										
									db.collection('userlist').find({username: user.username}).toArray(function (err, data){
													
											if(data.length){

												if(data[0]._id == req.body._id){

													//console.log('username not change and without change password')
											
													delete user._id;
											
													db.collection('userlist').updateById(req.params.id, user, function(err, data){
													res.json(data);
													});

												}else if(data[0]._id !== req.body._id){

													//console.log('username is change and without change password')
													res.send(301, {
															message: 
															'Error: Sorry, cannot create account "'+ 
															username +'" already taken.'});
												}

													
											}else if(captain == null){
												res.send(301, {
														message: 'Error: Baranggay Captain is empty!'});
											}else if(username == null){
													res.send(301, {
															message: 'Error: Username is empty.'});
											}else if(themes == null){
													res.send(301, {
															message: 'Error: Please Select themes.'});
											}else{

												//console.log('data is change without conflict and without change password');

												delete user._id;
										
												db.collection('userlist').updateById(req.params.id, user, function(err, data){
												res.json(data);
												});	

											}

									});
											
								}

			}else if(req.accesscontrol === 'barangayaccesscontrol'){


					var user 				= req.body;
						user.createdbyId 	= req.user;
						user.createdbyadmin	= req.byusername;
						user.updateby		= req.byusername;

					var barangay = req.body.barangay,
						username = req.body.username,
						newpassword = req.body.pswrd,
						verifypassword = req.body.verifypassword

								if(newpassword){

									if(newpassword !== verifypassword){
								
										res.send(301, {
											message: 
											"Error: Password don't match!"});
									}else{
											
											 db.collection('userlist').find({username: user.username}).toArray(function (err, data){
													
													
													if(data.length){

														if(data[0]._id == req.body._id){

															//console.log('username not change but password change')
															user.password = hash(req.body.pswrd);
															delete user._id;
															delete user.pswrd 
															delete user.verifypassword 
															db.collection('userlist').updateById(req.params.id, user, function(err, data){
															res.json(data);
															});

														}else if(data[0]._id !== req.body._id){

															//console.log('username is change but password change')
															res.send(301, {
																	message: 
																	'Error: Sorry, cannot create account "'+ 
																	username +'" already taken.'});
														}

															
													}else{

														//console.log('data is change without conflict but password change');

														user.password = hash(req.body.pswrd);
														delete user._id;
														delete user.pswrd 
														delete user.verifypassword 

														db.collection('userlist').updateById(req.params.id, user, function(err, data){
														res.json(data);
														});	

													}

											});
										
									}
								}else{
										
									db.collection('userlist').find({username: user.username}).toArray(function (err, data){
													
											if(data.length){

												if(data[0]._id == req.body._id){

													//console.log('username not change and without change password')
											
													delete user._id;
											
													db.collection('userlist').updateById(req.params.id, user, function(err, data){
													res.json(data);
													});

												}else if(data[0]._id !== req.body._id){

													//console.log('username is change and without change password')
													res.send(301, {
															message: 
															'Error: Sorry, cannot create account "'+ 
															username +'" already taken.'});
												}

													
											}else{

												//console.log('data is change without conflict and without change password');

												delete user._id;
										
												db.collection('userlist').updateById(req.params.id, user, function(err, data){
												res.json(data);
												});	

											}

									});
											
								}

			}
	})
	.delete(function (req, res){
		db.collection('userlist').removeById(req.params.id, function(){
			res.json(null);
		});
	});

router
	.use(cors())
	.use(bodyparser.urlencoded({limit: '10000mb'}))
    .use(bodyparser.json({limit: '10000mb'}))
    .use(methodOverride())
	.route('/account')
	.get(function (req, res){
		db.collection('userlist').findById(req.user, function (err, data){
			res.json(data);
		})
	});
	
module.exports = router;